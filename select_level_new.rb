include Gosu
include Globals

class SelectLevel < Menu
  BORDER = 100
  SPACING = 180
  TEXT_H = 30
  TEXT_C = 0xff995522

  def initialize(window, level_type)
    super(window)
    @background = window.images["menu_background2.png"]
    @mouse.set_img('pointer.png')
    @window = window
    @level_type = (level_type == 'level') ? 'lev' : 'cus'
    @levels = Dir.entries("levels/")
    @levels.delete_if {|level| level.split('.')[1] != level_type }
    @levels.sort! {|a, b| a[/\d+/].to_i <=> b[/\d+/].to_i }
    cols = (WIDTH - 2*BORDER) / SPACING
    rows = (HEIGHT - 2*BORDER) / SPACING
    i = 0
    rows.times do |y|
      cols.times do |x|
        break if i >= @levels.size
        @buttons << TextButton.new(window, self, (x+1)*SPACING, 
                 (y+1)*SPACING, @levels[i].split('.')[0], TEXT_H, 
                 TEXT_C, @levels[i])
        i += 1
      end
    end
    @buttons << TextButton.new(window, self, WIDTH - 200, HEIGHT - 60,
                    "RETURN", TEXT_H, TEXT_C, 'return')
  end

  def activate_button(name)
    kill
    case name
    when /lev/, /cus/
      GameScreen.new(@window, @level_type, name)
    end
  end

end
