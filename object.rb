class GameObject

  def initialize(screen)
    screen.add_object(self)
  end

  def update
  end

  def draw
  end

  def gravity(intensity)
  end

end
