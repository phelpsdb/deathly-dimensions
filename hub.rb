include Gosu
include ZOrder
include Globals

# displays the player's status (lives, health, bullets, coins, jumps, etc.)

class Hub

  def initialize(window, screen, player)
    @window = window
    @screen = screen
    @player = player
    @health_meter = Image.load_tiles(window, "images/health_meter.png",
                                     200, 30, true)
    @bullet_image = Image.load_tiles(window, "images/bullet_player.png",
                                     40, 7, true)
    @player_image = Image.load_tiles(window, "images/player.png",
                                     52, 70, true)
    @coin_image = Image.load_tiles(window, "images/item_coin.png",
                                   25, 25, true)
    @jump_image = Image.load_tiles(window, "images/item_sjump.png",
                                   30, 30, true)
    @gold_key = Image.new(window, "images/key_gold.png", true)
    @silver_key = Image.new(window, "images/key_silver.png", true)
    @bronze_key = Image.new(window, "images/key_bronze.png", true)
    @meter_x, @meter_y = 20, 20
    @hub_font = Font.new(window, Gosu.default_font_name, 40)
    @update_interval = ""
    @intervals = []
    @prev_time = milliseconds
    @player_mode = 0
    @factor_x = 1
    @coin_mode = 0
    @health_color = Color.new(255, 0, 255, 0)
  end

  def update
    @factor_x = @player.health.to_f / 1000
    @factor_x = 0 if @player.health <= 0
    @health_color.green = (@factor_x * 255).to_i
    @health_color.red = 255 - @health_color.green
    #maintain the brightness
    @health_color.value = 1.0
    @bullet_text = "X #{@player.bullets}"
    @coin_text = "X #{@player.coins}"
    @jump_text = "X #{@player.power_jumps}"
    @coin_mode = milliseconds / 100 % @coin_image.size

    # calculate fps, max and target speed = 60fps
    @accum_fps ||= 0
    @cur_sec ||= 0
    @accum_fps += 1
    now = milliseconds
    if @cur_sec != now / 1000
      @cur_sec = now / 1000
      @update_interval = "fps: #{@accum_fps}"
      @accum_fps = 0
    end

    # old fps calc
=begin
    @intervals << (1000 / (milliseconds - @prev_time))
    if @intervals.size >= 20
      sum = 0
      @intervals.each {|int| sum += int }
      sum /= @intervals.size
      @update_interval = "fps: #{sum}"
      @intervals = []
    end
    @prev_time = milliseconds
=end

    @player_mode = @player.mode  # for life images
  end
  
  def draw
    @health_meter[1].draw(@meter_x, @meter_y, HUB, @factor_x, 1, @health_color)
    @health_meter[0].draw(@meter_x, @meter_y, HUB)
    @bullet_image[0].draw(@meter_x + 230, @meter_y + 10, HUB, 2, 2)
    @coin_image[@coin_mode].draw(@meter_x + 230, @meter_y + 35, HUB, 2, 2)
    @jump_image[0].draw(@meter_x + 410, @meter_y, HUB, 2, 2)
    @hub_font.draw(@bullet_text, @meter_x + 330, @meter_y, TEXT)
    @hub_font.draw(@coin_text, @meter_x + 300, @meter_y + 40, TEXT)
    @hub_font.draw(@jump_text, @meter_x + 475, @meter_y, TEXT)
    @hub_font.draw(@update_interval, WIDTH - 200, 50, TEXT)
    @player.lives.times do |x|
      @player_image[@player_mode].draw(x*25 + 20, 70, HUB, 0.5, 0.5)
    end
    @player.keys.each do |key|
      case key
      when :key_gold
        @gold_key.draw(@meter_x, @meter_y + 100, HUB, 0.7, 0.7)
      when :key_silver
        @silver_key.draw(@meter_x + 20, @meter_y + 100, HUB, 0.7, 0.7)
      when :key_bronze
        @bronze_key.draw(@meter_x + 40, @meter_y + 100, HUB, 0.7, 0.7)
      end
    end
  end

end

