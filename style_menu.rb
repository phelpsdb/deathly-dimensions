include Globals
include Gosu

class StyleMenu < Menu
  RES_FAC = WIDTH.to_f/1024.0 # 1024 is standard
  BACK_X = WIDTH/8
  TILE_X = WIDTH.to_f/2.9
  MUSIC_X = WIDTH - WIDTH/4
  GRAVITY_X = (WIDTH/3)
  GRAVITY_Y = HEIGHT - 150 * RES_FAC
  START_Y = HEIGHT/10 * RES_FAC
  SPACING = 80*RES_FAC

  def initialize(window, editor)
    super(window)
    @background = window.editor_images['background_select.png']
    @window, @editor = window, editor
    @mouse.set_img('pointer.png')
    @info = "Set the level Background, Tileset, Music, and Gravity."
    @gravity = editor.map.styles[3].to_f
    @grav_info = "Gravity Acceleration: #{@gravity} px/frame**2"
    @font = Font.new(window, Gosu.default_font_name, (25*RES_FAC).to_i)
    rows = 4
    cols = 2
    i = 1
    cols.times do |x|
      rows.times do |y|
        next if i > Styles::BACKGROUNDS.size
        filename = Styles::BACKGROUNDS[i.to_s]
        @buttons << ImageButton.new(window, self, BACK_X*(x + 1) - 50*RES_FAC,
          (SPACING + 20)*(y+1) + START_Y, 
          "e_images/" + filename.split(/_/, 2)[1], filename)
        i += 1
   # Styles::BACKGROUNDS.each do |i, filename|
   #   @buttons << ImageButton.new(window, self, BACK_X, 
   #         SPACING*i.to_i + START_Y, "e_images/" + filename.split(/_/, 2)[1], filename)
      end
    end
    Styles::TILESETS.each do |i, filename|
      @buttons << ImageButton.new(window, self, TILE_X, 
                SPACING*i.to_i + START_Y, filename, filename)
    end
    Styles::MUSIC.each do |i, filename|
      @buttons << TextButton.new(window, self, MUSIC_X, 
            SPACING*i.to_i + START_Y, filename, #.split(/./, 2)[0], 
            13, 0xff000099, filename)
    end
    @buttons << TextButton.new(window, self, GRAVITY_X, GRAVITY_Y,
                               "Greater", 15, 0xff009900, 'greater')
    @buttons << TextButton.new(window, self, GRAVITY_X, GRAVITY_Y + 40,
                               "Less", 15, 0xff009900, 'less')
    @buttons << TextButton.new(window, self, GRAVITY_X + 70, GRAVITY_Y,
                               "Much Greater", 15, 0xff009900, 'mgreater')
    @buttons << TextButton.new(window, self, GRAVITY_X + 70, GRAVITY_Y + 40,
                               "Much Less", 15, 0xff009900, 'mless')
    @buttons << TextButton.new(window, self, 100 * RES_FAC, HEIGHT - 70, 'RETURN', 25, 0xff000000, 'return')
  end

  def aux_draw
    @font.draw(@info, BACK_X, 20, ZOrder::TEXT)
    @font.draw(@grav_info, GRAVITY_X - 70, GRAVITY_Y - 50, ZOrder::TEXT)
  end

  def activate_button(name)
    case name
    when /background/
      @editor.set_style(:background, name)
    when /tiles_/
      @editor.set_style(:tileset, name)
    when /.ogg/
      @editor.set_style(:music, name)
    when 'greater', 'less'
      @gravity += (name == 'greater') ? 0.01 : -0.01
      @gravity = ((@gravity * 100).round).to_f / 100
      @grav_info = "Gravity Acceleration: #{@gravity} px/frame**2"
      @editor.set_style(:gravity, @gravity)
    when 'mgreater', 'mless'
      @gravity += (name == 'mgreater') ? 0.10 : -0.10
      @gravity = ((@gravity * 100).round).to_f / 100
      @grav_info = "Gravity Acceleration: #{@gravity} px/frame**2"
      @editor.set_style(:gravity, @gravity)
    when /return/
      kill
    end
  end 

end
