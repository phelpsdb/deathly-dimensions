include Gosu
include Globals

# for fade-in/fade-out when starting, restarting, or ending a level

class Overlay

  def initialize(window, screen)
    @image = Image.new(window, "images/overlay.png", true)
    @color = Color.new(0x00ffffff)
    @phase = :static
    @fade = 3
  end

  def update
    case @phase
    when :removing_black
      unless @color.alpha < @fade
        @color.alpha -= @fade
      else
        @color.alpha = 0
        @phase = :static
      end
    when :blackening
      unless @color.alpha > 225 - @fade
        @color.alpha += @fade
      else
        @color.alpha = 255
        @phase = :static
      end
    end
  end

  def draw
    @image.draw(0, 0, ZOrder::OVERLAY, 1, 1, @color)
  end

  def remove_black
    @phase = :removing_black
    @color.alpha = 255
    @color.red = 0
    @color.green = 0
    @color.blue = 0
  end

  def blacken
    @phase = :blackening
    @color.alpha = 0
    @color.red = 0
    @color.green = 0
    @color.blue = 0
  end

end
