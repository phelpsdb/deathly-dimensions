include Gosu

# The counterpart to rmphelps' explosion engine

 class Particle
   #GRAVITY_DIV = 20.00

   def initialize(window, screen, image,  x, y, z, mode = :normal)
     @window = window
     @screen = screen
     @image = image
     @fadeout = 5
     @factor = 0.3
     @color = Color.new(255, 255, 69 + rand(186), 0)
     @x, @y, @z = x, y, z
     @velocity = rand * 4
     @image_angle = @angle = rand(360)
     @grav_x, @grav_y = 0, 0
   end

   def update
     if @color.alpha <= @fadeout
       false
     else
       @color.alpha -= @fadeout
       @vy = offset_y(@angle, @velocity)
       @vx = offset_x(@angle, @velocity)
       #@vy += @grav_y
       @x += @vx
       @y += @vy
       @velocity -= 0.03 unless @velocity <= 0
       true
     end
   end

   def draw(screen_x, screen_y)
     @image.draw_rot(@x - screen_x, @y - screen_y, @z, @image_angle, 0.5, 0.5, @factor, @factor, @color)
   end

   def gravity(intensity)
     #@grav_y += intensity.to_f/GRAVITY_DIV
     #@grav_x += offset_x(180, intensity)
   end

end
