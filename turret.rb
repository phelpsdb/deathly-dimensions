include Gosu
include Globals

class Turret < GameObject
  attr_reader :x, :y, :width, :height

  def initialize(window, screen, x, y, dir)
    super(screen)
    screen.add_target(self)
    @window = window
    @screen = screen
    @x, @y = x + 12.5, y + 12.5
    @dir = dir
    @images = Image.load_tiles(window, "images/turret.png", 25, 25, true)
    @width, @height = 25, 25
    @particle = Image.new(window, "images/particle.png", true)
    @bullet_image = Image.load_tiles(window, "images/bullet_turret.png", 
                                     40, 7, true)
    case dir
    when :right
      @angle = 90
    when :down
      @angle = 180
    when :left
      @angle = 270
    when :up
      @angle = 0
    end
    @mode = 0
    @charge = rand(100)
    @destroyed = false
  end

  def update
    @mode = milliseconds/100 % @images.size
    @charge += 1
    shoot
    return false if @destroyed
    true
  end

  def draw(screen_x, screen_y)
    spot_x, spot_y = @x - screen_x, @y - screen_y
    if spot_x > -25 and spot_x < WIDTH and spot_y>-25 and spot_y<HEIGHT-20
      @images[@mode].draw_rot(spot_x, spot_y, ZOrder::OBJECT, @angle - 90,
                              0.5 , 0.5)
    end
  end

  def shoot
    return if @charge < 100
    Bullet.new(@window, @screen, @x, @y, @angle, @bullet_image, 
              :enemy_bullet)
    @charge = 0
  end

  def die
    return if @destroyed
    @screen.remove_target(self)
    @destroyed = true
    Explosion.new(@window, @screen, @particle, @x, @y, ZOrder::OBJECT) 
    @window.samples['explosion.ogg'].play(@window.game_ops['soundfx'])
  end

  def obj_id
    :turret
  end
end
