require './background'
require 'enumerator'
include Gosu
include Globals

# Course class grabbed and modified from Cptn Ruby's Map class

class Course
  attr_reader :width, :height, :level, :music
  attr_accessor :course
  
  def initialize(window, screen, level, mode)
    mode = (mode == 'test') ? 'custom' : mode
    map = "levels/#{mode}#{level}.txt"
    #map = "levels/test_run.txt"
    screen.kill and return if !File.exists?(map)
    lines = File.new(map).readlines {|line| line.chop}
    styles = (lines[1].split(/~~/, 2)[1]).split(/::/, 4)
    tile_image = Styles::TILESETS[styles[0]]
    background = Styles::BACKGROUNDS[styles[1]]
    @music = Styles::MUSIC[styles[2]]
    screen.intensity = styles[3].to_f
    items = {'+' => :health_small, 'i' => :bullet_item, 'j' => :super_jump,
            'g' => :key_gold, 's' => :key_silver, 'b' => :key_bronze,
            'c' => :coin}
    texts = {'t' => '0', 'T' => '1', 'y' => '2', 'Y' => '3'}
    changes = {'|' => '0', '\\' => '1', '/' => '2', '?' => '3'}
    @background = Background.new(self, Image.new(window, "images/#{background}", true))
    @course_tiles = Image.load_tiles(window, "images/#{tile_image}",
      Tiles::WIDTH, Tiles::HEIGHT, true)
    @level = level
    @height = lines.size
    @width = lines[0].size
    @course = Array.new(@height) do |y|
      screen_y = y * Tiles::HEIGHT
      Array.new(@width) do |x|
        screen_x = x * Tiles::WIDTH
        case lines[y][x, 1]
        when "0", "1", "2", "3"
          Warp.new(screen, screen_x, screen_y, lines[y][x, 1])
          nil
        when "%"
          Tiles::BLOCK[rand(4)]
        when "\""
          Tiles::TOP_LAYER[rand(4)]
        when "_"
          Tiles::EMPTY
        when 't', 'T', 'y', 'Y'
          Trigger.new(window, screen, screen_x, screen_y, 200, 200) {TextBox.new(window, screen, level.to_s + '-' + texts[lines[y][x, 1]])}
          nil
        when '|', '\\', '/', '?'
          Trigger.new(window, screen, screen_x, screen_y, 100, 100) {CourseChange.new(window, screen, self, level.to_s + "-" + changes[lines[y][x, 1]]) }
          nil
        when ">"
          Dog.new(window, screen, screen_x, screen_y, self)
          nil
        when "&"
          Guard.new(window, screen, screen_x, screen_y, 300, self)
          nil
        when "$"
          Guard.new(window, screen, screen_x, screen_y, 600, self)
          nil
        when "}"
          Turret.new(window, screen, screen_x, screen_y, :right)
          nil
        when "{"
          Turret.new(window, screen, screen_x, screen_y, :left)
          nil
        when "m"
          Turret.new(window, screen, screen_x, screen_y, :up)
          nil
        when "w"
          Turret.new(window, screen, screen_x, screen_y, :down)
          nil
        when '+', 'i', 'j', 'g', 's', 'b', 'c'
          Item.new(window, screen, items[lines[y][x, 1]], screen_x, screen_y)
          nil
        when "x"
          DeathSpot.new(window, screen, screen_x, screen_y, :fall)
          nil
        when "l"
          LavaTile.new(window, screen, screen_x, screen_y)
          nil
        when "L"
          LavaTile.new(window, screen, screen_x, screen_y, :surface)
          nil
        when "G"
          Door.new(window, screen, screen_x, screen_y, @level + 1, :key_gold)
          nil
        when "!"
          Door.new(window, screen, screen_x, screen_y, "1", :key_silver, :warp)
          nil
        when "@"
          Door.new(window, screen, screen_x, screen_y, "2", :key_bronze, :warp)
          nil
        when "#"
          Door.new(window, screen, screen_x, screen_y, "3", :key_silver, :warp)
          nil
        else
          nil
        end
      end
    end
  end


  def draw(screen_x, screen_y)
    @background.draw(screen_x, screen_y)

    start_x, start_y = screen_x/Tiles::WIDTH, screen_y/Tiles::HEIGHT
    offs_x, offs_y = screen_x % Tiles::WIDTH, screen_y % Tiles::HEIGHT
    (offs_y == 0 ? HEIGHT/Tiles::HEIGHT + 1 : HEIGHT/Tiles::HEIGHT + 2).times do |y|
      (offs_x == 0 ? WIDTH/Tiles::WIDTH + 1 : WIDTH/Tiles::WIDTH + 2).times do |x|
    #(offs_y == 0 ? 31 : 32).times do |y|
    #  (offs_x == 0 ? 41 : 42).times do |x|
        tile = @course[y + start_y][x + start_x]
        if tile
          @course_tiles[tile].draw(x * Tiles::WIDTH - offs_x, y * Tiles::HEIGHT - offs_y, ZOrder::COURSE)
        end
      end
    end

  end

  #returns true if a block is located at a given coordinate
  def ground?(x, y)
    y < 0 || @course[y/Tiles::HEIGHT][x/Tiles::WIDTH]
  end

  def overlaps_ground?(left, top, right, bottom)
    @course[(top/Tiles::HEIGHT)..(bottom/Tiles::HEIGHT)].any? do |row|
      row[(left/Tiles::WIDTH)..(right/Tiles::WIDTH)].any?
    end
  end

end
