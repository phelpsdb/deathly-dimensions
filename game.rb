include Gosu

class GameScreen < Screen
  attr_reader :doors, :course, :jukebox, :screen_x, :screen_y
  attr_accessor :player, :intensity, :text_box, :options
  
  def initialize(window, mode, level)
    super(window)
    @objects = []
    @tasks = []
    @targets = []
    @doors = []
    @warps = []
    @triggers = []
    @window = window
    @mode = mode
    @level = level
    @intensity = 1
    @screen_x, @screen_y = 0, 0
    @frozen = false
    @player = Player.new(window, self)
    @overlay = Overlay.new(window, self)
    @jukebox = Jukebox.new(window)
    @screen_text = ""
    @screen_font = Font.new(window, Gosu.default_font_name, 80)
    start_level(@level)
  end

  def update
    unless @frozen
      @objects.reject! {|obj| !obj.update}
      check_collision
      @tasks.reject! {|task| !task.tick}
    end
    @text_box.update if @text_box
    @jukebox.update
    @screen_x = [[@player.x - WIDTH/2, 0].max,(@course.width*25-WIDTH)].min
    @screen_y = [[@player.y - HEIGHT/2, 0].max,((@course.height-5)*25-HEIGHT)].min
    @meter.update
    @overlay.update
  end

  def check_collision
    @objects.each do |obj|
      obj.gravity(@intensity)
      id = obj.obj_id
      case id
      when :dog
        if (obj.x - @player.x).abs < (obj.width/2 + @player.width/2) and
            (obj.y - @player.y).abs < (obj.height/2 + @player.height/2)
          @player.damage(170) if obj.awake?
        end
      when :guard
        if (obj.x - @player.x).abs < (obj.width/2 + @player.width/2) and
            (obj.y - @player.y).abs < (obj.height/2 + @player.height/2)
          @player.damage(1000)
        end
      when :bullet
        @targets.each do |target|
          if (obj.x-target.x).abs < (obj.width/2+target.width/2) and 
            (obj.y - target.y).abs < (obj.height/2 + target.height/2) then
            target.die and obj.vanish
          end
        end
      when :enemy_bullet
        if (obj.x - @player.x).abs < (obj.width/2 + @player.width/2) and
          (obj.y - @player.y).abs < (obj.height/2 + @player.height/2) then
          @player.damage(200) and obj.vanish
        end
      when :guard_shot
        if (obj.x - @player.x).abs < (obj.width/2 + @player.width/2) and
             (obj.y - @player.y).abs < (obj.height/2 + @player.height/2) 
          @player.die(:lava) and obj.vanish
        end
      when :health_small, :bullet_item, :key_gold, :key_silver, :coin,
        :key_bronze, :super_jump
        obj.obtain if @player.check_item(obj.x, obj.y, obj.width/2, obj.height/2, id)
      when :death_spot, :lava
        @player.die(obj.death) if ((obj.x + obj.width/2) - @player.x).abs < (obj.width/2 + @player.width/2.5) and
        ((obj.y + obj.height/2) - @player.y).abs < (obj.height/2 + @player.height/2.5)
        @targets.each do |targ|
          targ.die if ((obj.x + obj.width/2) - targ.x).abs < 100 and 
          ((obj.y + obj.height/2) - targ.y).abs < 30
        end
      when :door
        obj.open if ((obj.x + obj.width/2)-@player.x).abs < obj.width*2 and
        ((obj.y + obj.height/2) - @player.y).abs < obj.width*2
      end
    end
    @triggers.each do |trig|
      if (trig.x + trig.width/2 - @player.x).abs < (trig.width/2 + @player.width/2) and
        (trig.y + trig.height/2 - @player.y).abs < (trig.height/2 + @player.height/2) then
        trig.activate
      end
    end
  end

  def bdraw
  end

  def draw
    @objects.each {|obj| obj.draw(@screen_x, @screen_y)}
    @course.draw(@screen_x, @screen_y)
    @meter.draw
    @screen_font.draw_rel(@screen_text, WIDTH/2, HEIGHT/2, ZOrder::EVENT, 0.5, 0.5)
    @overlay.draw
    @text_box.draw if @text_box
  end

  def button_down(id)
    @player.button_down(id)
    if id == Button::KbEscape
      @jukebox.pause_music
      Pause.new(@window, self)
    end
    if id == Button::KbReturn
      if @text_box
        @text_box.close 
      end
    end
    if id == Button::KbF1
      @jukebox.stop_music
      kill
    end
  end

  def button_up(id)
    @player.button_up(id)
  end

  def freeze
    @frozen = true
  end

  def resume
    @frozen = false
  end

  def load_course
    @course = Course.new(@window, self, @level, @mode)
    @meter = Hub.new(@window, self, @player)
    @jukebox.play_music(@course.music)
    @jukebox.fade(:in)
  end

  def warp_player(destination)
    @warps.each do |warp|
      if destination == warp.id
        @player.warp_to warp.x + 12.5, warp.y + 12.5
        @player.normalize
      end
    end
  end

  def add_object(obj)
    @objects << obj
  end

  def add_task(task)
    @tasks << task
  end
  
  def add_target(target)
    @targets << target
  end

  def remove_target(target)
    @targets -= [target]
  end

  def add_door(door)
    @doors << door
  end

  def add_warp(warp)
    @warps << warp
  end

  def add_trigger(trig)
    @triggers << trig
  end

  def remove_trigger(trig)
    @triggers -= [trig]
  end

  def game_over
    @screen_text = "GAME OVER"
    Task.new(self, :wait => 300) {kill}
  end

  def game_end
    @screen_text = "YOU WIN!"
    Task.new(self, :wait => 300) {kill}
  end

  def custom_end
    Task.new(self, :wait => 120) {kill}
  end

  def start_level(level, from_door = false)
    if @mode == 'custom' || @mode == 'test'
      if from_door == true
        custom_end 
        return
      end
    elsif !File.exists?("levels/#{@mode}#{level}.txt")
      game_end
      return
    end
    overlay_action :remove_black
    @level = level
    @objects = []
    @tasks = []
    @targets = []
    @doors = []
    @warps = []
    @triggers = []
    @objects << @player
    load_course
    @player.start_level((@mode == 'test') ? true : false)
    warp_player("0")
  end

  def restart_level
    @objects = []
    @tasks = []
    @targets = []
    @doors = []
    @warps = []
    @triggers = []
    @objects << @player
    if @player.lives == 0
      game_over
    else
      overlay_action :remove_black
      load_course
      @player.respawn
      warp_player("0")
    end
  end

  def overlay_action(action)
    case action
    when :blacken
      @overlay.blacken
    when :remove_black
      @overlay.remove_black
    end
  end

end
