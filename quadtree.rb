include Globals

class Quadtree

  def initialize(screen)
    @screen = screen
    course = screen.course
    @bounds = [course.width/2 + 1, course.height/2 + 1] # 1   1 | 2
  end

  def update
    @screen.objects.each do |obj|
      if obj.x < @bounds[0]
        classify(:left, obj)
      elsif obj.x == @bounds[0]
        classify(:left, obj)
        classify(:right, obj)
      else
        classify(:right, obj)
      end
    end

  end

  def classify(side, obj)
    case side
    when :left
      if obj.y <= @bounds[1]
        @q_1 << obj
      elsif obj.y == @bounds[1]
        @q_1 << obj
        @q_3 << obj
      else
        @q_3 << obj
      end
    when :right
      if obj.y <= @bounds[1]
        @q_2 << obj
      elsif obj.y == @bounds[1]
        @q_2 << obj
        @q_4 << obj
      else
        @q_4 << obj
      end
    end
  end

end
