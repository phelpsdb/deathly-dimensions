include Gosu
include Globals

# lavatiles are 5 spaces wide save speed

class LavaTile < GameObject
  attr_reader :x, :y, :width, :height

  def initialize(window, screen, x, y, type = :plain)
    super(screen)
    case type
    when :plain
      image = "images/lava_plain.png"
    when :surface
      image = "images/lava_surface.png"
    end
    @screen = screen
    @x, @y = x, y
    @width, @height = Tiles::WIDTH*5, Tiles::HEIGHT
    @images = Image.load_tiles(window, image, @width, @height, true)
    @mode = 0
  end

  def update
    true
  end

  def draw(screen_x, screen_y)
    spot_x, spot_y = @x - screen_x, @y - screen_y
    if spot_x > - @width and spot_x < WIDTH and
      spot_y > - @height and spot_y < HEIGHT
      @mode = milliseconds / 70 % @images.size
      @images[@mode].draw(spot_x, spot_y, ZOrder::SECONDARY_COURSE)
    end
  end

  def obj_id
    :lava
  end

  def death
    :lava
  end

end
