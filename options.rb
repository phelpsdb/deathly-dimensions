include Gosu
include Globals
TH = 25
TC = 0xff003300
SP = HEIGHT/9
X = WIDTH/6
Y = HEIGHT/6

class Options < Menu

  def initialize(window)
    super(window)
    @background = window.images["menu_background.png"]
    @buttons << TextButton.new(window, self, X, SP*0 + Y, "Sound", TH, TC, "sound")
    @buttons << TextButton.new(window, self, X, SP*1 + Y, "Game Tips", TH, TC, "tips")
    @buttons << TextButton.new(window, self, X, SP*2 + Y, "Resolution", TH, TC, "resolution")
    @buttons << TextButton.new(window, self, X, SP*3 + Y, "Config", TH, TC, "config")
    @buttons << TextButton.new(window, self, X, SP*4 + Y, "Set to Defaults", TH, TC, "defaults")
    @buttons << TextButton.new(window, self, X, SP*5 + Y, "Save and Return", TH, TC, "return")
  end

  def activate_button(name)
    case name
    when "sound"
      SoundMenu.new(@window)
    when "tips"
      TipsMenu.new(@window)
    when "defaults"
      OptsHandler.set_defaults
      @window.game_ops = OptsHandler.load_options
    when "resolution"
      ResMenu.new(@window)
    when "config"
      ConfigMenu.new(@window)
    when "return"
      OptsHandler.save_options(@window.game_ops)
      kill
    end
  end

end

class SoundMenu < Menu

  def initialize(window)
    super(window)
    @background = window.images["menu_background.png"]
    @meter = window.images["meter.png"]
    @meter_cage = window.images["meter_cage.png"]
    @font = Font.new(window, Gosu.default_font_name, 40)
    @fx_factor = window.game_ops['soundfx']
    @ms_factor = window.game_ops['music']
    @buttons << TextButton.new(window, self, X, SP*3 + Y, "Return", TH, TC, "return")
    @buttons << TextButton.new(window, self, X, Y + 10, "<", TH, TC, "fxdown")
    @buttons << TextButton.new(window, self, X+260, Y+10, ">", TH, TC, "fxup")
    @buttons << TextButton.new(window, self, X, Y + SP + 30, "<", TH, TC, "msdown")
    @buttons << TextButton.new(window, self, X+260, Y + SP + 30, ">", TH, TC, "msup")
  end

  def update
    @buttons.each do |button|
      button.mouse_over if mouse_on_button?(button)
    end
  end

  def aux_draw
    @meter_cage.draw(X + 50, Y + 20, ZOrder::TEXT)
    @meter_cage.draw(X + 50, Y + SP + 40, ZOrder::TEXT)
    @font.draw_rel("Soundfx", X + 150, Y, ZOrder::TEXT, 0.5, 0.5, 1, 1, TC)
    @font.draw_rel("Music", X + 150, Y + 23 + SP, ZOrder::TEXT, 0.5, 0.5, 1, 1, TC)
    @meter.draw(X + 50, Y + 20, ZOrder::BUTTON, @fx_factor)
    @meter.draw(X + 50, Y + SP + 40, ZOrder::BUTTON, @ms_factor)
  end

  def activate_button(name)
    ops = @window.game_ops
    case name
    when "fxdown"
      ops['soundfx'] -= 0.1 unless ops['soundfx'] < 0.1
      @fx_factor = ops['soundfx']
    when "fxup"
      ops['soundfx'] += 0.1 unless ops['soundfx'] > 0.9
      @fx_factor = ops['soundfx']
    when "msdown"
      ops['music'] -= 0.1 unless ops['music'] < 0.1
      @ms_factor = ops['music']
    when "msup"
      ops['music'] += 0.1 unless ops['music'] > 0.9
      @ms_factor = ops['music']
    when "return"
      kill
    end
  end

end

class TipsMenu < Menu

  def initialize(window)
    super(window)
    @background = window.images["menu_background.png"]
    @font = Font.new(window, Gosu.default_font_name, 40)
    @ind_text = (@window.game_ops['tips'] == 1.0) ? "Activated" : "Deactivated"
    @buttons << TextButton.new(window, self, X, SP*0 + Y, "Activate", TH, TC, "on")
    @buttons << TextButton.new(window, self, X, SP*1 + Y, "Deactivate", TH, TC, "off")
    @buttons << TextButton.new(window, self, X, SP*2 + Y, "Return", TH, TC, "return")
  end
  
  def aux_draw
    @font.draw_rel(@ind_text, WIDTH - 20, HEIGHT/2, ZOrder::TEXT, 1.0, 0.5, 1, 1, TC)
  end

  def activate_button(name)
    case name
    when "on"
      @window.game_ops['tips'] = 1.0
      @ind_text = "Tips Activated"
    when "off"
      @window.game_ops['tips'] = 0.0
      @ind_text = "Tips Deactivated"
    when "return"
      kill
    end
  end

end

class ResMenu < Menu

  def initialize(window)
    super(window)
    @window = window
    @background = window.images["menu_background.png"]
    @font = Font.new(window, Gosu.default_font_name, 20)
    @full = (@window.game_ops['full']==1.0) ? "Activated" : "Deactivated"
    @cur_res = "#{@window.game_ops['width'].to_i} x #{@window.game_ops['height'].to_i}"
    @buttons << TextButton.new(window, self, X, SP*0 + Y, "640 x 480", TH, TC, "640")
    @buttons << TextButton.new(window, self, X, SP*1 + Y, "800 x 600", TH, TC, "800")
    @buttons << TextButton.new(window, self, X, SP*2 + Y, "1024 x 768", TH, TC, "1024")
    @buttons << TextButton.new(window, self, X, SP*3 + Y, "Fullscreen", TH, TC, "full")
    @buttons << TextButton.new(window, self, X, SP*4 + Y, "Return", TH, TC, "return")
  end

  def aux_draw
    @font.draw("#{@full} fullscreen", 5, 40, ZOrder::TEXT, 1, 1, TC)
    @font.draw("Selected: #{@cur_res}", 5, 10, ZOrder::TEXT, 1, 1, TC)
    @font.draw("Game must be restarted for new resolution to take effect.",
                   5, HEIGHT - 40, ZOrder::TEXT, 1, 1, TC)
  end

  def activate_button(name)
    case name
    when "640"
      @window.game_ops['width'] = 640
      @window.game_ops['height'] = 480
      @cur_res = "#{640} x #{480}"
    when "800"
      @window.game_ops['width'] = 800
      @window.game_ops['height'] = 600
      @cur_res = "#{800} x #{600}"
    when "1024"
      @window.game_ops['width'] = 1024
      @window.game_ops['height'] = 768
      @cur_res = "#{1024} x #{768}"
    when "full"
      full = @window.game_ops['full']
      full = (full - 1.0).to_i.to_f.abs
      @window.game_ops['full'] = full
      @full = (full == 1.0) ? "Activated" : "Deactivated"
    when "return"
      kill
    end
  end

end

class ConfigMenu < Menu
  TH = TH-5
  SP = SP-10

  def initialize(window)
    super(window)
    @background = window.images["menu_background.png"]
    @window = window
    @font = Font.new(window, Gosu.default_font_name, 20)
    @buttons << TextButton.new(window, self, X, SP*0 + Y/2, 
                "Jump: #{window.button_id_to_char(window.game_ops['jump'])}",
                TH, TC, "jump")
    @buttons << TextButton.new(window, self, X, SP*1 + Y/2, 
      "Dash Left: #{window.button_id_to_char(window.game_ops['dash_left'])}",
                TH, TC, "dash_left")
    @buttons << TextButton.new(window, self, X, SP*2 + Y/2, 
    "Dash Right: #{window.button_id_to_char(window.game_ops['dash_right'])}",
                TH, TC, "dash_right")
    @buttons << TextButton.new(window, self, X, SP*3 + Y/2, 
              "Run: #{window.button_id_to_char(window.game_ops['run'])}",
                TH, TC, "run")
    @buttons << TextButton.new(window, self, X, SP*4 + Y/2, 
              "Sneak: #{window.button_id_to_char(window.game_ops['sneak'])}",
                TH, TC, "sneak")
    @buttons << TextButton.new(window, self, X, SP*5 + Y/2, 
              "Shoot: #{window.button_id_to_char(window.game_ops['shoot'])}",
                TH, TC, "shoot")
    @buttons << TextButton.new(window, self, X, SP*6 + Y/2, 
    "Super Jump: #{window.button_id_to_char(window.game_ops['super_jump'])}",
                TH, TC, "super_jump")
    @buttons << TextButton.new(window, self, X, SP*7 + Y/2, 
    "Enter Door: #{window.button_id_to_char(window.game_ops['enter'])}",
                TH, TC, "enter")
    @buttons << TextButton.new(window, self, X, SP*8 + Y/2, "Return", TH, TC, "return")
  end

  def aux_draw
  end

  def activate_button(name)
    if name == 'return'
      kill
    else
      kill
      PressButton.new(@window, name)
    end
  end

end

class PressButton < Screen
  C = Color.new(0xffffffff)

  def initialize(window, name)
    super(window)
    @background = window.images["menu_background.png"]
    @font = Font.new(window, Gosu.default_font_name, 30)
    @name = name
    @window = window
  end

  def draw
    @background.draw_as_quad(0, 0, C, WIDTH, 0, C, WIDTH, HEIGHT,
                             C, 0, HEIGHT, C, ZOrder::BACKGROUND)
    @font.draw_rel("Press a button to be used for the action #{@name.gsub("_", " ")}", WIDTH/2, HEIGHT/2, ZOrder::TEXT, 0.5, 0.5, 1, 1, TC)
  end

  def button_down(id)
    @window.game_ops[@name] = id
    OptsHandler.save_options(@window.game_ops)
    ConfigMenu.new(@window)
    kill
  end

end
