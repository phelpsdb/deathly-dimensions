
# rmphelps' task engine operates to delay block activation

class Task
  def initialize(screen, options, &block)
    screen.add_task(self)
    @block = block
    @counter = options[:wait]
  end

  def tick
    if @counter <= 0
      @block.call
      return false
    end
    @counter -= 1
    true
  end
end
