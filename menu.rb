include Gosu

# all game menus are < Menu

class Menu < Screen
  C = Color.new(0xffffffff)

  def initialize(window)
    super(window)
    @window = window
    @mouse = MousePointer.new(window, self)
    @buttons = []
    # background must be initialized in all subclasses before "super"
  end
  
  def update
    @buttons.each do |button|
      button.mouse_over if mouse_on_button?(button)
    end
  end

  def draw
    @buttons.each {|btn| btn.draw}
    @background.draw_as_quad(0, 0, C, WIDTH, 0, C, WIDTH, HEIGHT,
                             C, 0, HEIGHT, C, ZOrder::BACKGROUND)
    @mouse.draw
    aux_draw
  end

  def aux_draw
    # to be defined by inheriting subclasses
  end

  def button_down(id)
    if id == Button::MsLeft then clicked end
    if id == Button::KbEscape then kill end
  end

  def clicked
    @buttons.each do |button|
      if mouse_on_button?(button)
        activate_button(button.name)
      end
    end
  end

  def mouse_on_button?(button)
    mx, my = @window.mouse_x, @window.mouse_y
    x, y = button.x, button.y
    width, height = button.dimensions[0..1]
    if mx >= x and my >= y and mx <= x + width and my <= y + height
      return true
    else
      return false
    end
  end

  def activate_button(name)
  end

end
