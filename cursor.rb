
# 3 guesses as to what this does. For menus and the Level Constructor

class MousePointer
  attr_reader :x, :y

  def initialize(window, screen)
    @window = window
    @screen = screen
    @image = window.images['pointer.png']
    @x = window.mouse_x
    @y = window.mouse_y
  end

  def draw
    # draw mouse image centered on target -- easy to tell where blocks go
    @image.draw_rot(@window.mouse_x, @window.mouse_y, ZOrder::MOUSE, 0, 0.5, 0.5)
  end

  def set_img(img, index = 0)
    @image = @window.images[img]
  end

end
