require 'rubygems'
#require 'ruby-prof'    # I tried Ruby-prof to see what might be slowing
require 'gosu'          #                         the game down the most.
require './opts_handler'
require './globals'
require './object'
require './entity'
require './course'
require './screen'
require './button'
require './cursor'
require './menu'
require './edit_menu'
require './style_menu'
require './instructions'
require './game_menu'
require './editor'
require './select_type'
require './select_level'
require './options'
require './pause'
require './game'
require './credits'
require './task'
require './overlay'
require './hub'
require './jukebox'
require './textbox'
require './trigger'
require './change'
require './explosion'
require './particle'
require './bullet'
require './player'
require './dog'
require './guard'
require './item'
require './turret'
require './door'
require './warp'
require './death_spot'
require './lavatile'
include Gosu
include Button
include Globals

#  *** THIS IS YOUR ENTRY POINT FOR THE GAME AND THE LEVEL CONSTRUCTOR ***

class GameWindow < Window
  attr_accessor :samples, :images, :editor_images, :time, :game_ops

  def initialize
    @game_ops = OptsHandler.load_options
    fullscreen = (@game_ops['full'] == 1.0) ? true : false
    #fullscreen = (ARGV[0] == 'fullscreen') ? true : false
    super(WIDTH, HEIGHT, fullscreen)
    #@game_ops = {"music" => 1.0, "soundfx" => 1.0, "tips" => true}
    @screens = []
    @time = 0
    @samples = Hash.new {|hash, file| hash[file] = Sample.new(self, 
                                                       "soundfx/#{file}") }
    @images = Hash.new {|hash, file| hash[file] = Image.new(self,
                                                 "images/#{file}", true) }
    @editor_images = Hash.new {|hash, file| hash[file] = Image.new(self,
                              "images/e_images/#{file}", true) }
    self.caption = "Deathly Dimensions"
    GameMenu.new(self)
  end

  def update
    @current_screen = @screens.last
    @current_screen.update
    if @current_screen.ended?
      @screens.pop
      if @screens.length == 0
        close
      end
    end
    @time += 1
  end
  
  def draw
    @current_screen.draw
  end

  def button_down(id)
    @current_screen.button_down(id)
  end

  def button_id_to_char(id)
    id = id.to_i
    if super(id) =~ /[A-Za-z_\[\]\\=`,.';\/]/ or super(id) =~ /\d{1,1}/
      super(id)
    else
      special_buttons = {
        KbDown => "down", KbUp => "up", KbLeft => "left", KbRight => "right",
        KbLeftShift => "left shift", KbRightShift => "right shift",
        KbLeftControl => "left ctrl", KbRightControl => "right ctrl",
        KbLeftAlt => "left alt", KbRightAlt => "right alt",
        KbBackspace => "backspace", KbTab => "tab", KbSpace => "Spacebar",
        KbInsert => "insert", KbHome => "home", KbEnter => "enter",
        KbReturn => "return", MsLeft => "mouse left", 
        MsRight => "mouse right", MsWheelDown => "mouse wheel down",
        MsWheelUp => "Mouse wheel up", KbPageDown => "page down",
        KbPageUp => "page up"
      }
      if special_buttons[id]
        return special_buttons[id]
      else
        return "custom button"
      end
    end
  end

  def button_up(id)
    @current_screen.button_up(id)
  end

  def add_screen(screen)
    @screens << screen
  end

  def activate_screen
    @current_screen = @screens.last
    @current_screen.activate
  end

end

# Profile the code
#result = RubyProf.profile do
  GameWindow.new.show
#end

# Print a graph profile to text
#printer = RubyProf::FlatPrinter.new(result)
#printer.print(File.new('flat_file.txt', 'w'), 0)
