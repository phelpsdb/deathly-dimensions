include Gosu
include Globals

class Pause < Menu
TH = 30
TC = 0xff005500
X = WIDTH/4
Y = 60

  def initialize(window, parent_screen)
    super(window)
    @parent = parent_screen
    @background = window.images['menu_background.png']
    @buttons << TextButton.new(window, self, X, Y + 60, "Continue", TH, TC, "continue")
    @buttons << TextButton.new(window, self, X, Y*2 + 60, "Options", TH, TC, "options")
    @buttons << TextButton.new(window, self, X, Y*3 + 60, "Die and Restart", TH, TC, "die")
    @buttons << TextButton.new(window, self, X, Y*4 + 60, "Quit", TH, TC, "quit")
  end

  def button_down(id)
    if id == Button::MsLeft then clicked end
    if id == Button::KbEscape 
      kill 
      @parent.jukebox.unpause_music
    end
  end

  def activate_button(name)
    case name
    when "continue"
      kill
      @parent.jukebox.unpause_music
    when "options"
      Options.new(@window)
    when "die"
      @parent.player.die
      kill
    when "quit"
      @parent.kill
      kill
    end
  end

end
