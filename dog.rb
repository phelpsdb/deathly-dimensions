include Gosu
include Globals

class Dog < Entity
  SLOW_DOWN_DOG = 0.96
  MAXSPEED_DOG = 10
  ACCELERATION_X_DOG = 0.4
  WAKE_UP_VEL_X = 3.1
  WAKE_UP_VEL_Y = 3

  def initialize(window, screen, starting_x, starting_y, course)
    super(window, screen)
    screen.add_target(self)
    @x, @y = starting_x + 12, starting_y
    @course = course
    @player = screen.player
    @images = Image.load_tiles(window, "images/dog.png", 65, 50, true)
    @width, @height = 44, 38 # for interobject collision
    @c_width, @c_height = 22, 48 # for course collision
    @color = Color.new(0xffffffff)
    @mode = @vx = @vy = 0
    @sprite_interval = 5
    @id = :dog
    @angle = 0
    @facing = :right
    @factor_x = -1
    @factor_y = 1
    @awake = false
    @dead = false
  end

  def update
    if !@dead
      normal_update
    else
      dying_update
    end
    return false if @color.alpha <= 6
    true
  end

  def normal_update
    update_position
    if distance(@player.x, @player.y, @x, @y) < 200
      if @player.vx.abs > WAKE_UP_VEL_X or @player.vy.abs > WAKE_UP_VEL_Y
        wake_up
      end
    end
    attack
    @vx *= SLOW_DOWN_DOG
    @vx = (@vx > MAXSPEED_DOG) ? MAXSPEED_DOG : @vx
    @mode = (!@awake) ? 3 : @mode
    if @vx.to_i != 0
      run
    elsif @vx.to_i == 0 and @awake
      stand
    end
    if distance(@player.x, @player.y, @x, @y) > 600
      fall_asleep
    end
    if !blocked? :down then @mode = 1 and @sprite_interval = 0 end
  end

  def dying_update
    @color.alpha -= 6
    @factor_y = (@vy > 0) ? -1 : 1
    (-@vy).to_i.times {@y-=1} if @vy < 0
    @vy.to_i.times {@y+=1} if @vy > 0
  end

  def run
    #@mode = (milliseconds / 120) % 3
    @sprite_interval += 1
    if @hit_ground
      @mode = 2
      @sprite_interval = 0
    end
    # glide pose becomes reality
    if @sprite_interval == 6
      @mode = 0
    end
    if @sprite_interval > 12
      @mode = 1
      @vy -= 5
      @sprite_interval = 0
    end
  end

  def stand
    @mode = 0
    @sprite_interval = 5
  end

  def attack
    return if !@awake
    if @player.x - @x < 0   # player is to the left of dog
      @vx -= ACCELERATION_X_DOG
      @factor_x = 1
    elsif @player.x - @x > 0    # player is to the right of dog
      @vx += ACCELERATION_X_DOG
      @factor_x = -1
    end
  end

  def wake_up
    return if @awake
    @awake = true
    @mode = 0
    @window.samples['dog_bark.ogg'].play(@window.game_ops['soundfx'])
  end

  def fall_asleep
    @awake = false
    @sprite_interval = 0
  end

  def awake?
    @awake
  end

  def die
    return if @dead
    @vx = 0
    @vy = -10
    @dead = true
    @window.samples['dog_die.ogg'].play(@window.game_ops['soundfx'])
  end

  def obj_id
    return @id if !@dead
  end

end
