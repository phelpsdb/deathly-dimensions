include Globals
include Gosu

class Instructions < Menu

  def initialize(window)
    super(window)
    @background = window.editor_images["background_instructions.png"]
    @mouse.set_img("pointer.png")
  end

  def clicked
    kill
  end

end
