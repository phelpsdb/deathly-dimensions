include Gosu
include Globals

# for the level constructor

class SelectType < Menu
  RES_FAC = WIDTH.to_f/1024.0 # 1024 is the standard x resolution
  BORDER = WIDTH/12*RES_FAC # border between rows of buttons and sides of window

  IMAGES_BY_TYPE = {
    '%' => 'brick.png', '"' => 'brick2.png', '_' => 'empty.png',
    '.' => 'eraser.png',
    '+' => 'bitem_smhealth.png', '0' => 'bstart.png',
    '1' => 'bwarp1.png', '2' => 'bwarp2.png', '3' => 'bwarp3.png',
    '>' => 'bdog.png', '&' => 'bguard.png', '$' => 'blguard.png',
    '}' => 'bturret_right.png', '{' => 'bturret_left.png',
    'm' => 'bturret_up.png', 'L' => 'blava_surface.png',
    'w' => 'bturret_down.png', 'c' => 'bitem_coin.png',
    'b' => 'bkey_bronze.png', 's' => 'bkey_silver.png',
    'g' => 'bkey_gold.png', 'j' => 'bitem_sjump.png',
    'i' => 'bitem_bullet.png',
    'x' => 'bdeath_spot.png', 'l' => 'blava_plain.png', 
    'G' => 'bdoor_brown.png', '!' => 'bdoor_silver.png',
    '@' => 'bdoor_bronze.png', '#' => 'b3door_silver.png' 
  }

  def initialize(window, editor)
    super(window)
    @background = window.editor_images['background_select.png']
    @editor = editor
    @mouse.set_img('pointer.png')
    columns, rows = 6, 5
    width = (WIDTH - BORDER*2) / columns #space available for each button 
    height = (HEIGHT - BORDER*2) / rows  #
    length = BUTTON_TYPES.size
    i = 0
    rows.times do |y|
      columns.times do |x|
        if i < length
          bx = BORDER + width * x
          by = BORDER + height * y
          @buttons << ImageButton.new(window, self, bx, by, 
              "e_images/" + IMAGES_BY_TYPE[BUTTON_TYPES[i]], BUTTON_TYPES[i])
          i += 1
        end
      end
    end
  end

  def activate_button(id)
    @editor.set_type(id)
    kill
  end

end
