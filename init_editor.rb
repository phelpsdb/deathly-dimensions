begin
  require 'rubygems'
rescue LoadError
end
require 'gosu'
require 'opts_handler'
require 'globals'
require 'cursor'
require 'screen'
require 'menu'
require 'background'
require 'edit_menu'
require 'style_menu'
require 'instructions'
require 'button'
require 'select_type'
require 'editor'
include Gosu
include Globals

#  *** THIS IS YOUR ENTRY POINT FOR THE LEVEL CONSTRUCTOR ***

class GameWindow < Window
  attr_accessor :images

  def initialize
    super(WIDTH, HEIGHT, false)
    @images = Hash.new {|hash, filename| hash[filename] = Image.new(self, "images/e_images/#{filename}", true) }
    @screens = []
    EditMenu.new(self)
  end

  def update
    @current_screen = @screens.last
    @current_screen.update
    if @current_screen.ended?
      @screens.pop
      if @screens.length == 0
        close
      end
    end
  end
  
  def draw
    @current_screen.draw
  end

  def button_down(id)
    @current_screen.button_down(id)
  end

  def button_up(id)
    @current_screen.button_up(id)
  end

  def add_screen(screen)
    @screens << screen
  end

  def activate_screen
    @current_screen = @screens.last
    @current_screen.activate
  end

end

GameWindow.new.show
