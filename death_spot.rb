include Gosu
include Globals

# this is that "fall off the edge of the cliff and die" spot.

class DeathSpot < GameObject
  attr_reader :x, :y, :width, :height

  def initialize(window, screen, x, y, death_type)
    super(screen)
    @window = window
    @screen = screen
    @death_type = death_type
    @x, @y = x + 12, y + 12
    @width, @height = Tiles::WIDTH*5, Tiles::HEIGHT
  end

  def update
    true
  end

  def draw(screen_x, screen_y)
  end

  def death
    @death_type
  end

  def obj_id
    :death_spot
  end
end
