include Gosu
include Globals

class Bullet < GameObject
  attr_reader :x, :y, :width, :height

  def initialize(window, screen, x, y, angle, image, id = :bullet)
    super(screen)
    @images = image
    @window = window
    @screen = screen
    @width = @images[0].width
    @height = @images[0].height
    @x, @y = x, y
    @id = id
    speed = (@id != :guard_shot) ? 10 : 26
    @angle = angle
    @vx = offset_x(@angle, speed)
    @vy = offset_y(@angle, speed)
    @mode = @length = 0
    @vanished = false
  end

  def update
    @x += @vx
    @y += @vy
    @mode = milliseconds / 200 % @images.size
    @length += 1
    check_collision
    return false if @vanished or @length >= 120
    true
  end

  def draw(screen_x, screen_y)
    spot_x = @x - screen_x
    spot_y = @y - screen_y
    if spot_x > - 100 and spot_x < WIDTH + 100
      if spot_y > - 100 and spot_y < HEIGHT + 100
        @images[@mode].draw_rot(@x - screen_x, @y - screen_y, ZOrder::OBJECT, @angle + 90, 0.5, 0.5)
      end
    end
  end

  def check_collision
    @vanished = true if @screen.course.ground?(@x, @y)
  end

  def vanish
    @vanished = true
  end

  def obj_id
    @id
  end

end
