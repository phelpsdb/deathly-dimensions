# originally rmphelps' explosion engine

class Explosion < GameObject
  def initialize(window, screen, image, x, y, z, mode = :normal)
    super(screen)
    @particles = []
    250.times {@particles << Particle.new(window, screen, image, x, y, z, mode)}
  end

  def update
    return false if @particles.empty?
    @particles.reject! {|particle| !particle.update}
    true
  end

  def draw(screen_x, screen_y)
    @particles.each {|p| p.draw(screen_x, screen_y)}
  end

  def gravity(intensity)
    @particles.each {|p| p.gravity(intensity)}
  end

  def obj_id
  end
end
