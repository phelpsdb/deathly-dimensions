include Gosu
include Globals

class Door < GameObject
  attr_reader :x, :y, :width, :height

  def initialize(window, screen, x, y, destination, key_type, mode = :level)
    super(screen)
    screen.add_door(self)
    case key_type
    when :key_gold
      image = "images/door_brown.png"
    when :key_silver
      image = "images/door_silver.png"
    when :key_bronze
      image = "images/door_bronze.png"
    end
    @door_images = Image.load_tiles(window, image, 50, 75, true)
    @x, @y = x, y - 50
    @width, @height = Tiles::WIDTH*2, Tiles::HEIGHT*3
    @destination = destination
    @screen = screen
    @key_type = key_type
    @mode = mode
    @phase = :shut
    @factor_x = 1
    @locked = true
    @door_z = ZOrder::OBJECT.to_f - 0.5
  end

  def update
    if @phase == :opening
      @factor_x -= 0.05
      @phase = :open if @factor_x <= -0.3
    elsif @phase == :closing
      @factor_x += 0.015
      @screen.jukebox.fade(:out) if @mode == :level
      if @factor_x >= 1
        if @mode == :level
          @screen.overlay_action :blacken
          Task.new(@screen, :wait =>180) {@screen.start_level(@destination, true)}
          @phase = :closed
        else
          @screen.warp_player(@destination)
          @phase = :shut
          @door_z = ZOrder::OBJECT.to_f - 0.5
        end
      end
    end
    true
  end

  def draw(screen_x, screen_y)
    spot_x, spot_y = @x - screen_x, @y - screen_y
    if spot_x > -50 and spot_x < WIDTH and spot_y > -75 and spot_y < HEIGHT
      @door_images[0].draw(spot_x, spot_y, @door_z ,@factor_x)
      @door_images[1].draw(spot_x, spot_y, ZOrder::SECONDARY_BCKGRND)
    end
  end

  def open
    return if @phase != :shut or @locked
    @phase = :opening
  end

  def close
    return if @phase == :shut 
    @phase = :closing
    @door_z = ZOrder::SECONDARY_OBJ
  end

  def key(type)
    @locked = false  if type == @key_type
  end

  def locked?
    @locked
  end

  def obj_id
    :door
  end

end
