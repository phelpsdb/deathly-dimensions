include Gosu
include Globals

# dinky little class that just has info on where to put the player

class Warp
  attr_reader :x, :y, :id

  def initialize(screen, x, y, id)
    screen.add_warp(self)
    @x, @y = x, y
    @id = id
  end

end
