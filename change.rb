include Gosu
include Globals

# this class is for creating or removing game tiles when triggered

class CourseChange

  def initialize(window, screen, course, code)
    sound = 'course_appear'
    #level, index = code.split(%r{\s*})[0..1]
    level, index = code.split('-', 2)[0..1]
    lines = File.new("levels/changes/#{level}.txt", 'r').readlines
    change_sets = lines[index.to_i].chomp.split('::')
    change_sets.each do |coords|
      x, y, z = coords.split('|')[0..2]
      if z == 'nil'
        z = nil 
        sound = 'course_disappear.ogg'
      else 
        z = z.to_i
        sound = 'course_appear.ogg'
      end
      course.course[y.to_i][x.to_i] = z
    end
    window.samples[sound].play(window.game_ops['soundfx'])
  end

end
