include Gosu

# handles the music playing

class Jukebox
  FADE = 0.005

  def initialize(window)
    @window = window
    # load the game music
    @music = Hash.new {|hash, key| hash[key] = Song.new(window, "soundfx/music/#{key}") }
    @looping = false
  end

  def play_music(music)
    begin
      @now_playing = @music[music]
    rescue LoadError
    end
    @now_playing.play(true)
    @now_playing.volume = @window.game_ops['music']
    @max_vol = @now_playing.volume # for fade in
    @playing = true
    @fade = :none
  end

  def update
    case @fade
    when :out
      vol = @now_playing.volume
      vol -= FADE
      if vol <= FADE
        vol = 0
        @fade = :none
        @now_playing.stop
      end
      @now_playing.volume = vol
    when :in
      vol = @now_playing.volume
      vol += FADE
      if vol >= @max_vol
        vol = @max_vol
        @fade = :none
      end
      @now_playing.volume = vol
    end
  end

  def stop_music
    @now_playing.stop
  end

  def pause_music
    @now_playing.pause
  end

  def unpause_music
    @now_playing.play(true)
    @now_playing.volume = @window.game_ops['music']
  end

  def fade(mode)  #modes are :in, :out. :out will automatically stop music
    @fade = mode
    @now_playing.volume = 0 if mode == :in
  end

end
