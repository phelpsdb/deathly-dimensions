include Gosu
include Globals

class Credits < Menu

  def initialize(window)
    super(window)
    @background = window.images['credits_background.png']
    @buttons << TextButton.new(window, self, WIDTH - 200, 100, "Return",
                             30, 0xff881122, 'return')
  end

  def activate_button(name)
    kill
  end

end
