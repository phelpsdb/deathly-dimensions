include Gosu
include Globals

ACCELERATION_X = 0.5
class Player < Entity
  attr_accessor :vx, :vy, :ax, :ay, :health, :bullets, :lives, :coins, :keys
  attr_accessor :course, :width, :height, :power_jumps
  attr_reader :max_speed, :pain_mode, :mode
  ACCELERATION_X = 0.5
  SLOW_DOWN = 0.945
  MAXSPEED = 4.5

  def initialize(window, screen)
    super(window, screen)
    @ticks = 0
    @max_speed = MAXSPEED
    @slow_down = SLOW_DOWN
    @acceleration_x = ACCELERATION_X
    @screen.player = self
    @images = Image.load_tiles(window, "images/player.png",52,70,true)
    @particle = Image.new(window, "images/particle.png", true)
    @bullet = Image.load_tiles(window, "images/bullet_player.png",40,7,true)
    @x, @y = 0, 0
    @mode = @angle = @vx = @vy = @ax = @ay = @charge = 0
    @color = Color.new(0xffffffff)
    @health = 1000
    @factor_x = @factor_y = 1
    @bullets = @coins = @power_jumps = 0
    @lives = 5
    @id = :player
    @keys = []
    @facing = :right
    @position = :airborne
    @pain_mode = :normal
    @sprite_interval = 0
    # pain modes are :normal, :hurting, :burning, :falling, :exploding, :still, :dead
    @invincible = @power_jump = false
    @width, @height = 30, 62
    @c_width, @c_height = 24, 60
  end

  def warp_to(x, y)
    @x, @y = x, y
  end

  def update
    if @pain_mode != :dead
      method(@pain_mode).call
      test_mode if @test_mode
      true
    else
      false
    end
  end

  def normal
    update_position
    @position = (blocked? :down) ? :grounded : :airborne
    check_buttons
    @vx *= @slow_down
    @vx = (@vx > 0) ? @max_speed : (-@max_speed) if @vx.abs > @max_speed
    @factor_x = (@facing == :right) ? 1 : -1
    @charge += 1
    if @vx.to_i == 0 and @position == :grounded
      stand
    elsif @vx.to_i != 0 and @position == :grounded
      run
    elsif @position == :airborne
      jump_mode
    end
    invincible_update if @invincible
  end

  def still
    @mode = 14
    @vx = @vy = 0
  end

  def hurting
    update_position
    @vx *= @slow_down
    @mode = 13
  end

  def invincible_update
    @color.alpha = (milliseconds/100 % 2 == 0) ? 0 : 255
  end

  def exploding
    @angle += 20
    @factor_x *= 0.95
    @factor_y = @factor_x.abs
    @mode = 12
  end

  def burning
    normal
    @color.blue -= 4 unless @color.blue == 0
    @color.blue = 0 if @color.blue <= 4
    @color.green = @color.blue
    @color.alpha = @color.blue
  end

  def falling
    @factor_x = @factor_y = 0
    @vy = 0
  end

  def button_down(id)
    if id == @window.game_ops['dash_right'] or #KbRight or 
     id == @window.game_ops['dash_left'] #KbLeft
      @sprite_interval = milliseconds
    end
    #if @window.button_id_to_char(id) == "m"
    #  @max_speed = MAXSPEED + 5
    #end
  end

  def button_up(id)
    if id == @window.game_ops['run'] or id == @window.game_ops['sneak']
      @max_speed = MAXSPEED
    end
  end

  def check_buttons
    if @window.button_down? @window.game_ops['dash_right'] #KbRight
      @vx += @acceleration_x
    end
    if @window.button_down? @window.game_ops['dash_left'] #KbLeft
      @vx -= @acceleration_x
    end
    if @window.button_down? @window.game_ops['shoot'] #KbSpace
      shoot
    end
    if @window.button_down? @window.game_ops['jump'] #KbRightAlt
      jump
    end
    if @window.button_down? @window.game_ops['super_jump'] #KbRightControl
      super_jump
    end
    if @window.button_down? @window.game_ops['enter'] #KbUp
      enter_door
    end
    if @window.button_down? @window.game_ops['run'] #@window.char_to_button_id("m")
      @max_speed = MAXSPEED + 5 unless @position == :airborne
    end
    if @window.button_down? @window.game_ops['sneak'] #@window.char_to_button_id(",")
      @max_speed = MAXSPEED - 2 unless @position == :airborne
    end
  end

  def stand
    @mode = 0
  end

  def run
    #increase sprite speed depending on velocity
    calc = 100 - (@vx.abs).to_i*6
    #mode = (milliseconds / calc) % 7
    #sprite interval makes smoother run entrance, 
    #still working on run exit, though.
    mode = ((milliseconds - @sprite_interval) / calc) % 7
    @mode = mode + 1
  end

  def jump_mode
    if @vy < -8
      @mode = 8
    elsif @vy < -4 and @vy >= -8
      @mode = 9
    elsif @vy < 0 and @vy >= -4
      @mode = 10
    elsif @vy <= 4 and @vy >= 0 
      @mode = 11
    elsif @vy > 4
      @mode = 12
    end
  end

  def jump
    if @position == :grounded
      jump_v = -17 - (@vx.abs/2).to_i
      @vy = jump_v
      @window.samples['jump.ogg'].play(@window.game_ops['soundfx'])
    end
  end

  def super_jump
    return if @power_jumps == 0
    if @position == :grounded
      jump_v = -30 - (@vx.abs/2).to_i
      @vy = jump_v
      @window.samples['jump.ogg'].play(@window.game_ops['soundfx'])
      @power_jumps -= 1
    end
  end

  def shoot
    return if @charge < 20 or @bullets == 0
    angle = (@facing == :right) ? 90 : 270
    Bullet.new(@window, @screen, @x, @y, angle, @bullet)
    @charge = 0
    @bullets -= 1
    @window.samples['shoot.ogg'].play(@window.game_ops['soundfx'])
  end

  def normalize
    @vx = 0
    @vy = 0
    @mode = 0
    @factor_x = (@facing == :right) ? -1 : 1
    @factor_y = 1
    @pain_mode = :normal
    @max_speed = MAXSPEED
    @color = Color.new(0xffffffff)
  end

  def damage(health_loss)
    return if @pain_mode != :normal or @invincible # only recieve damage
    @pain_mode = :hurting                          #       from norm mode
    @vx = (@facing == :right) ? -4 : 4
    @health -= health_loss
    if @health > 0
      @window.samples["ouch#{%w(1 2 3 4)[rand(4)]}.ogg"].play(@window.game_ops['soundfx'])
      Task.new(@screen, :wait => 20) do 
        @pain_mode = :normal if @pain_mode == :hurting
        @invincible = true
        Task.new(@screen, :wait => 60) do
          @invincible = false
          @color.alpha = 255
        end
      end
    else
      die
    end
  end

  def die(method = :normal)
    return if @pain_mode != :normal and @pain_mode != :hurting
    @screen.jukebox.stop_music
    @health = 0
    @window.samples['die_song.ogg'].play(@window.game_ops['music'])
    if method == :normal 
      @window.samples['die_explode.ogg'].play(@window.game_ops['soundfx'])
      @pain_mode = :exploding
      @vx = 0
      Task.new(@screen, :wait => 55) do 
        Explosion.new(@window, @screen, @particle, @x, @y, ZOrder::OBJECT)
        @window.samples['explode.ogg'].play(@window.game_ops['soundfx'])
        @pain_mode = :dead
        @lives -= 1
        Task.new(@screen, :wait => 120) {@screen.overlay_action :blacken}
        Task.new(@screen, :wait => 240) {@screen.restart_level}
      end
    elsif method == :fall
      @window.samples["#{%w(die_fall die_fall2)[rand(2)]}.ogg"].play(@window.game_ops['soundfx'])
      @pain_mode = :falling
      @vx = 0
      Task.new(@screen, :wait => 200) do 
        @pain_mode = :dead
        @lives -= 1
        @screen.overlay_action :blacken
        Task.new(@screen, :wait => 120) {@screen.restart_level}
      end
    elsif method == :lava
      @pain_mode = :burning
      @window.samples['burn.ogg'].play(@window.game_ops['soundfx'])
      Task.new(@screen, :wait => 60) do
        @pain_mode = :dead
        @lives -= 1
        @screen.overlay_action :blacken
        Task.new(@screen, :wait => 180) {@screen.restart_level}
      end
    end
  end

  def check_item(x, y, width, height, type)
    if (@x-x).abs < (width+20) and (@y-y).abs < (height+30)
      case type
      when :health_small
        @health += 200
        @health = (@health > 1000) ? 1000 : @health
      when :bullet_item
        @bullets += 1
      when :super_jump
        @power_jumps += 1
      when :key_gold, :key_silver, :key_bronze
        @keys << type
        unlock_doors
      when :coin
        @coins += 1
        @lives += 1 if @coins % 100 == 0
      end
      true
    else
      false
    end
  end

  def unlock_doors
    @keys.each do |type|
      @screen.doors.each {|door| door.key(type)}
    end
  end

  def enter_door
    return if @pain_mode == :still
    @screen.doors.each do |door|
      if ((door.x + door.width/2)-@x).abs < door.width/2 and
        ((door.y + door.height/2) - @y).abs < door.width/2 and
      #if ((door.x+25) - @x).abs < 15 and ((door.y+35) - @y).abs < 15 and 
          !door.locked?
        door.close
        @pain_mode = :still
        @x, @y = door.x + 25, door.y + 43
        @vx = 0
        @vy = 0
      end
    end
  end

  def start_level(test = false)
    @course = @screen.course
    @bullets = 0
    @power_jumps = 0
    @keys = []
    @health = 1000
    @test_mode = test
    test_mode if @test_mode
    unlock_doors
    normalize
  end

  def respawn
    @bullets = 0
    @health = 1000
    @coins = 0
    @angle = 0
    @power_jumps = 0
    @course = @screen.course
    unlock_doors
    normalize
  end

  # used in the level constructor
  def test_mode
    @keys = [:key_gold, :key_silver, :key_bronze]
    @lives = 5
    @health = 1000
    @bullets = 10
    @power_jumps = 10
  end

end
