
# background generator by rmphelps
class Background
  def initialize(game, image)
    @game = game
    @image = image
    @sf = Globals::SCROLL_FACTOR # Background scrolls slower than course
  end

  def draw(view_x, view_y)
    start_x = ((view_x.round/@sf) / @image.width).floor - 1
    start_y = ((view_y.round/@sf) / @image.height).floor - 1
    #start_x = (view_x / @image.width).round - 1   (if background scrolls
    #start_y = (view_y / @image.height).round - 1       with the course)
    (start_x .. start_x + ((Globals::WIDTH.to_f / @image.width).round + 1)).each do |tx|
      (start_y .. start_y + ((Globals::HEIGHT.to_f / @image.height).round + 1)).each do |ty|
        rx = (tx * @image.width - (view_x/@sf)).floor
        ry = (ty * @image.height - (view_y/@sf)).floor
        #rx = (tx * @image.width - view_x).round
        #ry = (ty * @image.height - view_y).round
        @image.draw(rx, ry, ZOrder::BACKGROUND)
      end
    end
  end
end
