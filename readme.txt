***************************
*                         * 
* Deathly Dimensions V1.1 *
*                         *
***************************

Changes from V1.0

-Selectively improved graphics
-Resolution independence
-Tip turn-off option for those annoying text boxes
-Custom input configuration
-Level Constructor now accessible from the game itself
-Test mode in Level Constructor
-New levels, including easier ones
-Dog given much improved graphic as well as run pattern

---Setup:---

This game requires you to have Ruby 1.8.6 or greater installed and Gosu (www.libgosu.org). 
See also code.google.com/p/gosu for directions on getting started with gosu on Windows, OS X, and Linux.

To play the game, go to the dimensions directory in the command line and type "ruby main.rb" 
The game should start up if you have acquired the above software.

---System Requirements:---

I had a crappy windows XP computer and the game worked okay for me. 
Unless your computer is really old or "special," it should have the hardware.
Gosu uses OpenGL, therefore if the game runs slow on your good quality computer, 
you may consider among other troubleshooting tips to check your graphics drivers.
The game has been tested and is known to work on Macs. 
The game has been tested and is known to work on Ubuntu (Linux).
The game has been known to slow down from target 60fps when a level is too large or involving. 
If you wish to create your own levels, keep that in mind also.

---Gameplay:---

To beat each level, simply grab the gold key and exit through the brown door 
(not to be confused with the silver and bronze/orange doors that only warp the player to other parts of the level).

Watch out for dogs. They are asleep at first, but moving too quickly when they're nearby will wake them up. 
Try sneaking past them if you can, or else shoot 'em.

Turrets can shoot in any of the four cardinal directions. Time your moves or else shoot 'em.

Guards are the most worrysome. One hit from their guns and you're dead. 
Fortunately, they too can be destroyed with your own bullets. Don't get seen, don't get shot. Never touch one.

Collect 100 coins and gain an extra life (gee, where have we heard this before?).

what do powerups do? (no duh):
Super jumps let you jump really high.
Bullets let you kill destructible targets (dogs, turrets, and guards).
Health, surprisingly, increases your health.

HUGE TIP: The faster you are moving, the higher you can jump. This is VITAL to your survival in the game. 
Just like in Super Mario Bros, you should pretty much always hold down the run key.

The keyboard controls layout can be modified to your preferences in the options menu of the game.

---Default Keyboard Layout:---

Left:     Run Left
Right:    Run Right
Space:    Shoot (if you have bullets)
Alt:      Jump (hold "M" and run to jump higher)
Ctrl:     Super Jump (if you have any)
M:        Hold down to run faster
, or <    Hold down to sneak
Esc:      Pause
F1        Fast Quit
Return    Close textbox
Mouse     Useful only in menus

---Options Menu:---

Here you can adjust the game settings in several ways that did not exist in the previous version.
Your options can be saved so you don't have to reset them every startup.
Additionally, you can restore defaults to all options.

-Sound-
Default: full volume for both
Adjust the game volumes for soundfx and music.

-In Game Tips-
Default: Activated (HIGHLY recommended for newbies)
Controls whether or not text-box tips will appear during gameplay.
Turn these off only if you have mastered all skills or else are confident enough to handle any tricks on your own.
I created this option simply because the tips get very annoying if you've seen the same one a thousand times.

-Config-
Default: See default keyboard layout above.
Set your player controls to the inputs by clicking on an action and pressing the desired input.
The name of the button you pressed will appear next to the action it points to.
If the button name is not found, fear not. The game still knows which button you pressed,
it will just in that menu display "custom button."

-Resolution-
Default: 800x600, fullscreen deactivated.
Originally, the game was set at 1024x768. This was too big for some screens.
The game has now been given resolution independence, so set it to any of
three choices: 640 x 480, 800 x 600, 1024 x 768, fullscreen yes/no.
The selected option will only be applicable upon restarting the game.
The game was originally designed under 1024 x 768, so that is recommended if you can manage it.

-Set to Defaults-
Reset all options to the values found in opts.def
Your own customized options are located in the file opts.dol if you wish
to hack them manually.

********************
Level Constructor
********************

---Setup:---

How to enter: The Level Constructor is now accessible from the game itself. Click on the respective button in the main menu.

---Usage:---

The mouse is your main tool here. 
In the opening menu, select a level or an empty slot to edit.
First you will see an instructions screen. Click to move on.
If you need to go back to the instructions, just press "i"

IMPORTANT: Don't forget to save your levels by pressing the Spacebar. Always, always, always, always remember to save before you exit! Take it from me.

Exception Notice: It is highly recommended that you line the borders of your level with tiles (they can be empty tiles).
Failure to do so will result in an exception when an object goes out of bounds.
The exception will look something like:
./course.rb:127:in `ground?': undefined method `[]' for nil:NilClass (NoMethodError)
This just means that an object (anything from bullets to yourself) has passed out of the physical level boundaries.

Exceptions are made quite possible by allowing you to create your own levels. 
They are uncommon and easily avoided in your own custom levels.
If you get an unusual exception which you know is not because of your own tinkering,
it may be posted on the forum at www.libgosu.org/cgi-bin/mwf/topic_show.pl?tid=136

---Button Layout:---

Left Click:     This is the "enter," "yes," "put 'er there" button
Right Click:    Open the Object Selection menu.
Left Shift:     Lifesaving feature that, when held down along with Left Click,
                  will rapidly post the selected object. 
                  Good for filling in large sections without killing 
                  your index finger clicking all the time.
1:              Shortcut for brick 1
2:              Shortcut for brick 2
3:              Shortcut for empty (invisible) brick
4:              Shortcut for eraser
S:              Open the styles menu.
i:              Open the (i)nstructions menu.
-:              Add a row to the bottom of your level.
=:              Add a column to the right side of your level.
x:              Subtract a row from the bottom of your level.
c:              Subtract a column from the right side of your level.
Spacebar:       VERY IMPORTANT: Save your level
T:              NEW: test run your level in cheat mode. 
                  Quitting this mode will return you to your editing 
                  session for quick improvements.
Esc:            Exit the menu or else the editor. MAKE SURE YOU SAVE.

---Object Selection:---

Obviously, you will want dogs, guards, coins, and such in your level.
Just right click on the mouse to open this menu.

I believe that Mac users still have a "right click" function on the mouse of some sort, 
but if not then you can just hack into editor.rb and change it to some other button.

When you click on an object, the menu automatically closes and the mouse is transformed into that object. 
Click where you want to put the object.
Hold down left shift as well as left click to, in effect, "keep the pencil on the paper."
This makes it much easier to fill in large sections of brick with out constantly clicking.


---Styles Menu:---

By pressing the "S" key, you will open up the styles menu. 
There you will select the background, tileset, music, and gravity for your level.
Pretty cool, eh?

---Playing your levels:---

IMPORTANT: You will notice that the bottom five rows of your level seem to be cut off during gameplay
These rows STILL EXIST, they are merely HIDDEN from the camera during actual gameplay.
This allows you to fill that area with death pits ("oh no I fell off the cliff and died" pits).
This lets the player fall off and die in a smooth manner rather than a sudden disappearance.

To test run your level, save it (spacebar) and simply press "T." 
You will start from the position that you placed the "start" object. 
If this object does not exist, default is (0, 0).
Test run mode will also be in cheat mode: infinite lives, regenerating health,
infinite bullets, infinite super jumps, and a full set of keys.

If you want to play your level in normal mode, save it and exit the editor to the main menu.
From there, click on "Play Custom Levels."
Select your particular level and have fun.

---

The purpose of the Level Constructor is for you to share your devious tricks and traps with others.
Please post any worthy levels that you create on the forum. 
This makes them eligible to be put into the next game distribution with your name in the credits.

Special thanks to AmIMeYet for inspiring the level editor.

---

Give it all a shot and tell us what you think at:
http://www.libgosu.org/cgi-bin/mwf/topic_show.pl?tid=136

-phelps.db

