
class Screen
  def initialize(window)
    window.add_screen(self)
    window.activate_screen
    @tasks = []
    @active = false
    @ended = false
  end

  def update
  end

  def draw
  end

  def button_down(id)
  end

  def button_up(id)
  end

  def ended?
    @ended
  end

  def activate
    @active = true
    @ended = false
  end

  def kill
    @ended = true
  end

  def add_task(task)
    @tasks << task
  end

end
