include Gosu
include Globals

class Guard < Entity
  PATROL_MODES = [0, 1, 2]
  SHOOT_MODE = 3

  def initialize(window, screen, x, y, patrol_span, course)
    super(window, screen)
    screen.add_target(self)
    @x, @y = x + 25, y - 20
    @vx, @vy = 0, 0
    @patrol = [@x, @x + patrol_span]
    @player = screen.player
    @course = course
    @width, @height = 34, 88
    @c_width, @c_height = 30, 84
    @images = Image.load_tiles(window, "images/guard.png",100, 99, true)
    @particle_image = Image.new(window, "images/particle.png", true)
    @q_mark = Image.new(window, "images/q_mark.png", true)
    @bullet = Image.load_tiles(window, "images/bullet_guard.png",15,15,true)
    @mode = PATROL_MODES[0]
    @angle = 0
    @charge = @search = @over_shoot = 0
    @factor_x = -1
    @factor_y = 1
    @color = Color.new(0xffffffff)
    @id = :guard
    @facing = :right
    @patrol_mode = :patrol
    @dead = false
  end

  def update
    update_position
    if blocked? :left then @facing = :right end
    if blocked? :right
      @facing = :left 
      #puts 'hi'
    end
    @factor_x = (@facing == :right) ? -1 : 1
    if @patrol_mode != :dead
      method(@patrol_mode).call
      true
    else
      false
    end
  end

  def draw(screen_x, screen_y)
    @images[@mode].draw_rot((@x - screen_x).to_i, (@y - screen_y).to_i,
           ZOrder::OBJECT, @angle, 0.5, 0.5, @factor_x, @factor_y, @color)
    if @patrol_mode == :huh? and milliseconds/250 % 2 == 0
      @q_mark.draw_rot(@x-screen_x + 35*@factor_x, @y-screen_y - 30,
                     ZOrder::OBJECT, 0)
    end
  end

  def patrol
    @mode = PATROL_MODES[milliseconds / 100 % 3] 
    @charge = -15
    if @x >= @patrol[1] and @facing == :right
      @patrol_mode = :search
    elsif @x <= @patrol[0] and @facing == :left
      @patrol_mode = :search
    end
    @vx = (@facing == :right) ? 2 : -2
    @patrol_mode = :shoot if find_player
  end

  def shoot
    @vx = 0
    @mode = SHOOT_MODE
    @charge += 1
    if @charge >= 8 and @player.pain_mode != :dead
      angle = (@facing == :right) ? 90 : 270
      Bullet.new(@window, @screen, @x + 35*(-@factor_x), @y - 5, angle, 
                 @bullet, :guard_shot)
      @window.samples['shot_sound.ogg'].play(@window.game_ops['soundfx'])
      @charge = 0
    end
    @over_shoot += 1 unless find_player
    @patrol_mode = :huh? if @over_shoot >= 20
  end
  
  def search
    @vx = 0
    @search += 1
    @patrol_mode = :shoot and @search = 0 if find_player
    if @search >= 120
      @search = 0
      @facing = (@facing == :right) ? :left : :right if @patrol_mode == :search
      @patrol_mode = :patrol
    end
  end

  def huh?
    search
  end

  def find_player
    player_dir = :right if @x <= @player.x
    player_dir = :left if @x > @player.x
    if distance(@player.x, @player.y, @x, @y) < 500 and 
        (@player.y - @y).abs < 75 and player_dir == @facing
      @over_shoot = 0
      true
    else
      false
    end
  end

  def dying
    @color.blue -= 3
    @color.green = @color.blue
    @vx *= 0.99
    if @color.blue < 3
      Explosion.new(@window, @screen, @particle_image, @x, @y, ZOrder::OBJECT, :normal)
      @window.samples['explosion.ogg'].play(@window.game_ops['soundfx'])
      @patrol_mode = :dead
    end
  end

  def extra_draw(screen_x, screen_y)
    if @patrol_mode == :huh? and milliseconds/250 % 2 == 0
      @q_mark.draw_rot(@x-screen_x + 35*@factor_x, @y-screen_y - 30,
                     ZOrder::OBJECT, 0)
    end
  end

  def die
    @patrol_mode = :dying
    @screen.remove_target(self)
    @window.samples['guard_die.ogg'].play(@window.game_ops['soundfx'])
  end

end
