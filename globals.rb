module Globals
  opts = OptsHandler.load_options
  WIDTH, HEIGHT = opts['width'].to_i, opts['height'].to_i
  #WIDTH, HEIGHT = 1024, 768
  SCROLL_FACTOR = 2.0
end

module Levels
  LEV_ARRAY = ["noob_world", "guard_dogs", "guard_dogs_II",
  "turreteer", "pluto", "goomba", "isenguard", "leap_of_faith",
  "night_club", "dungeon", "doom"]
  LEV_HASH = []
end

module Tiles
  EMPTY = 0
  BLOCK = [1, 2, 3, 4]   # gives somewhat of randomness to tile appearance
  TOP_LAYER = [5, 6, 7, 8]
  WIDTH = 25
  HEIGHT = 25
end

module ZOrder
  BACKGROUND, SECONDARY_BCKGRND, COURSE, OBJECT, BUTTON, SECONDARY_COURSE,
  SECONDARY_OBJ, HUB, TEXT, OVERLAY, EVENT, MOUSE = *(0..20)
end

module Styles
  # ummm... yeah, these could definitely be arrays :/
  # I will fix that later.
  TILESETS = {
    '1' => 'tiles_soil.png', '2' => 'tiles_brick.png',
    '3' => 'tiles_stone.png', '4' => 'tiles_wood.png' }

  BACKGROUNDS = {
    '1' => 'background_bbco.png', '2' => 'background_woodpanel.png',
    '3' => 'background_purplebubs.png', '4' => 'background_sky.png',
    '5' => 'background_stars.png', '6' => 'background_sun.png',
    '7' => 'background_moon.png'  }

  MUSIC = {
    '1' => 'dangerous.ogg', '2' => 'fowl_waters.ogg',
    '3' => 'the_dark_revolution.ogg', '4' => 'revelations1.ogg',
    '5' => 'revelations2.ogg', '6' => 'revelations3.ogg' }
end
