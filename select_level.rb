include Gosu
include Globals

class SelectLevel < Menu
  RES_FAC = WIDTH.to_f/1024.0 # 1024 is the standard x resolution
  BORDER = (50*RES_FAC).to_i
  X_SP = 130
  Y_SP = 60
  TEXT_H = 20
  TEXT_C = 0xff995522

  def initialize(window, level_type)
    super(window)
    @background = window.images["menu_background2.png"]
    @mouse.set_img('pointer.png')
    @window = window
    @level_type = level_type
    @levels = Dir.entries("levels/")
    @levels.delete_if {|level| level !~ /#{level_type}/ }
    @levels.sort! {|a, b| a[/\d+/].to_i <=> b[/\d+/].to_i }
    cols = (WIDTH - 2*BORDER) / X_SP
    rows = (HEIGHT - 2*BORDER) / Y_SP
    i = 0
    rows.times do |y|
      cols.times do |x|
        break if i >= @levels.size
        @buttons << TextButton.new(window, self, (x+1)*X_SP - 65, 
                 (y+1)*Y_SP + 60, @levels[i], TEXT_H, TEXT_C, @levels[i])
        i += 1
      end
    end
    @buttons << TextButton.new(window, self, WIDTH - 200, HEIGHT - 60,
                    "RETURN", TEXT_H, TEXT_C, 'return')
  end

  def activate_button(name)
    kill
    case name
    when /.txt/
      GameScreen.new(@window, @level_type, name[/\d+/].to_i)
    end
  end

end
