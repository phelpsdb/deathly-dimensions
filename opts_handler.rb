require 'fileutils' # for set_defaults

class OptsHandler

  def self.load_options
    if !File.exists?("opts.dol") then self.set_defaults end
    optsf = File.new("opts.dol", "r")
    lines = optsf.readlines
    opts = Hash.new
    lines.each do |l|
      l.chop
      (opt, val) = l.split('::')
      opts[opt] = (((val.to_f*10).to_i).to_f)/10 # no flakey numbers
    end
    optsf.close
    return opts
  end

  def self.save_options(opts_hash)
    optsf = File.new("opts.dol", "w")
    opts_hash.each do |opt, val|
      val = (((val.to_f*10).to_i).to_f)/10 # no flakey numbers
      optsf.write("#{opt}::#{val}\n")
    end
    optsf.close
  end

  def self.set_defaults
    FileUtils::copy("opts.def", "opts.dol")
  end

end
