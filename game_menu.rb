include Gosu
include Globals
TEXT_HEIGHT = 20
TEXT_COLOR = 0xffccbb77

class GameMenu < Menu

  def initialize(window)
    super(window)
    @background = Image.new(window, "images/menu_background2.png", true)
    start_x = 100
    start_y = HEIGHT/5
    sp = HEIGHT/8
    @window = window
    @buttons << TextButton.new(window, self, start_x, start_y, 
                      "Start Adventure", TEXT_HEIGHT, TEXT_COLOR, 'level')
    @buttons << TextButton.new(window, self, start_x, start_y + sp, 
                  "Play Custom Levels", TEXT_HEIGHT, TEXT_COLOR, 'custom')
    @buttons << TextButton.new(window, self, start_x, start_y + sp*2, 
                  "Options", TEXT_HEIGHT, TEXT_COLOR, 'options')
    @buttons << TextButton.new(window, self, start_x, start_y + sp*3, 
                  "Level Constructor", TEXT_HEIGHT, TEXT_COLOR, 'editor')
    @buttons << TextButton.new(window, self, start_x, start_y + sp*4, 
                  "Credits", TEXT_HEIGHT, TEXT_COLOR, 'credits')
    @buttons << TextButton.new(window, self, start_x, start_y + sp*5, 
                                  "EXIT", TEXT_HEIGHT, TEXT_COLOR, 'exit')
  end

  def activate_button(name)
    @window.caption = "Deathly Dimensions"
    case name
    when 'exit'
      kill
    when 'level', 'custom'
      SelectLevel.new(@window, name)
    when 'options'
      Options.new(@window)
    when 'editor'
      EditMenu.new(@window)
    when 'credits'
      Credits.new(@window)
    end
  end

end
