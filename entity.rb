include Gosu
include Globals

#for player, dogs, guards, and hopefully bullets in the future.

class Entity < GameObject
  attr_reader :x, :y, :width, :height

  def initialize(window, screen)
    super(screen)
    @screen = screen
    @window = window
    @last_vy = 0
    @hit_ground = false
  end

  def update
  end

  def draw(screen_x, screen_y)
    spot_x = @x - screen_x
    spot_y = @y - screen_y
    if spot_x > - 100 and spot_x < WIDTH + 100
      if spot_y > - 100 and spot_y < HEIGHT + 100
        @images[@mode].draw_rot((@x - screen_x).to_i, (@y - screen_y).to_i,
           ZOrder::OBJECT, @angle, 0.5, 0.5, @factor_x, @factor_y, @color)
      end
    end
  end

  def update_position
    if @vx < 0
      @facing = :left
      (-@vx).to_i.times do
        if blocked? :left
          @vx = 0
          break
        else
          @x -=1
        end
      end
    elsif @vx > 0
      @facing = :right
      @vx.to_i.times do
        if blocked? :right
          @vx = 0
          break
        else
          @x +=1
        end
      end
    end

    if @vy < 0
      (-@vy).to_i.times do
        if blocked? :up
          @vy = 0
          break
        else
          @y -= 1
        end
      end
    elsif @vy > 0
      @vy.to_i.times do
        if blocked? :down
          @vy = 0
          break
        else
          @y += 1
        end
      end
    end
    
    # regulate @vy.abs < 1 depending on the direction of gravity
    if @screen.intensity >= 0
      @vy = 0 if blocked? :down and @vy > 0 and @vy < 1
    else
      @vy = 0 if blocked? :up and @vy > -1 and @vy < 0
    end

    # regulate animation when running against a wall
    if blocked?((@vx > 0) ? :right : :left) then @vx = 0 end

    # check if entity just landed
    if @last_vy > 0 and @vy == 0
      @hit_ground = true
    else
      @hit_ground = false
    end
    @last_vy = @vy
  end

  def blocked?(dir)
    coordinates = [@x - (@c_width/2), @y - (@c_height/2),
                   @x + (@c_width/2), @y + (@c_height/2)]
    case dir
    when :left
      coordinates[0] -= 1
      coordinates[2] -= 1
    when :right
      coordinates[0] += 1
      coordinates[2] += 1
    when :up
      coordinates[1] -= 1
      coordinates[3] -= 1
    when :down
      coordinates[1] += 1
      coordinates[3] += 1
    end
    @screen.course.overlaps_ground?(*coordinates)
  end

  def gravity(intensity)
    @vy += intensity
  end

  def obj_id
    @id
  end

end
