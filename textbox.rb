include Gosu
include Globals

# this is for those annoying help boxes that pop up when triggered to
# give the player tips and things. Yes, I know they're irritating,
# but you should really read them or else you won't know how to
# beat the game.

class TextBox

  def initialize(window, screen, text_call)
    return if window.game_ops['tips'] == 0.0
    screen.text_box = self
    screen.freeze
    @screen = screen
    #level, index = text_call.split(%r{\s*})[0..1]
    level, index = text_call.split('-', 2)[0..1]
    lines = []
    File.new("levels/info_texts/" + level + ".txt", 'r').each_line do |line|
      lines <<  line
    end
    text = lines[index.to_i]
    #text = GameText::TEXTS[text_call]
    @box_image = Image.new(window, "images/text_box.png", true)
    @text_obj = Image.from_text(window, text, default_font_name, 
                               (35 * WIDTH.to_f/@box_image.width).to_i, 0, 
            (WIDTH - 100.0*(WIDTH.to_f/@box_image.width)).to_i, :justify)
    @box_factor = 0
    @mode = :opening
  end

  def update
    if @mode == :opening
      @box_factor += 0.05
      @mode = :static if @box_image.width*@box_factor >= WIDTH - 10
    elsif @mode == :closing
      @box_factor -= 0.05
      if @box_factor <= 0
        @screen.text_box = false
        @screen.resume 
      end
    end
  end

  def draw
    @box_image.draw_rot(WIDTH/2, HEIGHT*0.8, ZOrder::HUB, 0, 0.5, 0.5, 
                        @box_factor, @box_factor)
    if @mode == :static
      @text_obj.draw(50 * @box_factor, HEIGHT - @box_image.height*@box_factor + 50*@box_factor, ZOrder::TEXT)
    end
  end

  def close
    @mode = :closing
  end

end

# I don't actually use this part anymore:

module GameText

TEXTS = {
  '1a' =>
    "You've been transported to another world! The infamous world of Lea Din! Now, you must fight your way, course to course in hope that you will survive to see your home again. Use the left and right keys to move along the course! Press the 'Alt' key to leap into the air! Beat the enemies, before they beat you! Bwahahahahahahahah! (press 'Return')",

  '1b' =>
    "Press and hold ',' while moving to sneak past the sleeping dog. But if you fail, press and hold 'M' to run for your life before you get eaten. Tip: the faster you are running, the higher you will jump. Remembering that will prove useful in the future.",

  '1c' =>
    "Use this bullet to shoot the dog over yonder. Press the space bar to shoot when he is in range.",

  '1d' =>
    "You need a key to open that door. Keep looking, it should be around here  somewhere.",

  '2a' =>
    "Watch out for turrets in this level. Hint: save your bullets for hard parts.",

  '2b' =>
    "This is a Super Jump. Guess what it does. That's right, jump to dizzying heights by pressing the 'Ctrl' button.",

  '3a' =>
    "Holy Cats! The gravity is out of whack! Don't jump when no ceiling is above, you'll fly out of the orbital sphere to your death!",

  '3b' =>
    "This lava doesn't look too friendly. If you don't believe me, go ahead and take a swim.",

  '3c' =>
    "Look out below!",

  '3d' =>
    "I told you to watch it! Now you're toast!",

  '4a' =>
    "Uh oh! A guard is up there! Don't get seen, don't get shot.",

  '5a' =>
    "Quick! Go up this tower fast, or you'll find a nasty surprise waiting for you!",

  '6a' =>
    "Leap of Faith: Do you trust your senses?"

}

end


