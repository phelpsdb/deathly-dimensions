include Gosu
include Globals

# this menu is the "select a level to edit" in the Level Constructor
class EditMenu < Menu
  TEXT_HEIGHT = 20
  TEXT_COLOR = 0xff000000
  BCK_C = Color.new(0xffffffff)
  RES_FAC = WIDTH.to_f/1024.0 # 1024 is the standard size
  
  BORDER = 70*RES_FAC
  def initialize(window)
    super(window)
    @background = window.editor_images['background_select.png']
    @font = Font.new(window, default_font_name, 20)
    @text = "Select a level to edit"
    @mouse.set_img('pointer.png')
    create_buttons
  end

  def draw
    @font.draw(@text, 40*RES_FAC, 40*RES_FAC, ZOrder::TEXT, 1, 1, 0xff00ff00)
    @buttons.each {|btn| btn.draw }
    @background.draw_as_quad(0, 0, BCK_C, WIDTH, 0, BCK_C, WIDTH, HEIGHT,
                             BCK_C, 0, HEIGHT, BCK_C, ZOrder::BACKGROUND)
    @mouse.draw
  end

  def create_buttons
    rows, columns = 10, 4
    width = (WIDTH - BORDER*1.5) / columns
    height = (HEIGHT - BORDER*1.5) / rows
    @buttons = []
    i = 0
    @levels = Dir.entries("levels/")
    @levels.delete_if {|file| file !~ /custom/}
    @levels.sort! {|a, b| a[/\d+/].to_i <=> b[/\d+/].to_i }
    rows.times do |y|
      columns.times do |x|
        if i < @levels.size
          @buttons << TextButton.new(@window, self, BORDER + width*x,
            BORDER + height*y, @levels[i], TEXT_HEIGHT, TEXT_COLOR, @levels[i])
          i += 1
        else
          @buttons << TextButton.new(@window, self, BORDER + width*x,
            BORDER + height*y, 'EMPTY', TEXT_HEIGHT, TEXT_COLOR, 'empty')
        end
      end
    end
  end

  def activate_button(name)
    case name
    when /custom/
      EditorMain.new(@window, name)
    when 'empty'
      EditorMain.new(@window, "custom#{@levels.size + 1}.txt")
      create_buttons
    end
  end

end
