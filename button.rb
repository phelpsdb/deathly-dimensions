include Gosu

# These two classes operate in menus as either text or pics

class TextButton
  attr_reader :name, :x, :y, :dimensions
  attr_accessor :text
  BORDER = 10

  def initialize(window, screen, x, y, text, text_height, text_color, name)
    @window, @screen, @x, @y, @name = window, screen, x.to_i, y.to_i, name
    @text, @t_color = text, text_color
    @image = window.images['button.png']
    @color = Color.new(0xffffffff)
    @font = Font.new(window, Gosu.default_font_name, text_height)
    t_width, t_height = @font.text_width(@text), text_height
    i_width, i_height = @image.width, @image.height
    @dimensions = [t_width + 2*BORDER, t_height + 2*BORDER]
    @factor_x = (t_width + 2*BORDER).to_f / i_width.to_f
    @factor_y = (t_height + 2*BORDER).to_f / i_height.to_f
  end

  def draw
    @image.draw(@x, @y, ZOrder::BUTTON, @factor_x, @factor_y, @color)
    @font.draw(@text, @x + BORDER, @y + BORDER, ZOrder::TEXT, 1, 1, @t_color)
    @color.green = @color.blue = 255
  end

  def mouse_over
    @color.green = @color.blue = 0
  end

end

class ImageButton
  attr_reader :name, :x, :y, :dimensions
  BORDER = 10

  def initialize(window, screen, x, y, image_name, name)
    @window, @screen, @x, @y, @name = window, screen, x, y, name
    @design = window.images[image_name]
    @image = window.images['button.png']
    @color = Color.new(0xffffffff)
    d_width, d_height = @design.width, @design.height
    i_width, i_height = @image.width, @image.height
    @dimensions = [d_width + 2*BORDER, d_height + 2*BORDER]
    @factor_x = (d_width + 2*BORDER).to_f / i_width.to_f
    @factor_y = (d_height + 2*BORDER).to_f / i_height.to_f
  end

  def draw
    @image.draw(@x, @y, ZOrder::BUTTON, @factor_x, @factor_y, @color)
    @design.draw(@x + BORDER, @y + BORDER, ZOrder::BUTTON)
    @color.green = @color.blue = 255
  end

  def mouse_over
    @color.green = @color.blue = 0
  end

end
