include Gosu

# sets off textboxes and course changes

class Trigger
attr_reader :x, :y, :width, :height

  def initialize(window, screen, x, y, width, height, &block)
    screen.add_trigger(self)
    @block = block
    @screen = screen
    @x, @y = x, y
    @width, @height = width, height
  end
  
  def activate
    @screen.remove_trigger(self)
    @block.call
  end

end
