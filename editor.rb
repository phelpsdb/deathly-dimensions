=begin
  Level constructor for Dimensions
  Enter this program through "ruby init_editor.rb"
  Structured by AmIMeYet
  Enhanced and implemented by phelps.db
  Version 1.0
  Shortcut Buttons:
    Left Mouse Button: Add Tile
    Right Mouse Button: Object selection menu
    1: Select Earth (#)
    2: Select Grass (")
    3: Select Empty (_)
    4: Erase
    S: Styles menu
    Spacebar: Save map (overwrite)   ---VERY IMPORTANT
    Escape: Exit to last screen   ---DON'T FORGET TO SAVE
=end

include Gosu
include Globals

  OBJ_IMGS = {'+' => "eitem_smhealth.png", '0' => 'start.png',
    '1' => 'warp1.png', '2' => 'warp2.png', '3' => 'warp3.png',
    '>' => 'edog.png', '&' => 'eguard.png', '$' => 'eguard.png',
    '}' => 'turret_right.png', '{' => 'turret_left.png', 
    'm' => 'turret_up.png',
    'w' => 'turret_down.png', 'c' => 'eitem_coin.png',
    'b' => 'ekey_bronze.png', 's' => 'ekey_silver.png',
    'g' => 'ekey_gold.png', 'j' => 'eitem_sjump.png', 
    'i' => 'eitem_bullet.png',
    'x' => 'death_spot.png', 'l' => 'lava_a.png', 'L' => 'lava_b.png',
    'G' => 'edoor_brown.png', '!' => 'edoor_silver.png',
    '@' => 'edoor_bronze.png', '#' => 'edoor_silver.png' }
  BUTTON_TYPES = ['%', '"', '_', '.', 'g', 's', 'b', 'c', 'i', 'j', '+',
    '{', '}', 'm', 'w', '>', '&', '$', 'x', 'L', 'l', '0', '1', '2', 
    '3', 'G', '!', '@', '#']


# CourseObject is a class to show items, enemies, warps and such
class CourseObject
  attr_reader :x, :y

  def initialize(window, screen, type, x, y)
    image = OBJ_IMGS[type]
    @window = window # save these two fields in case they are ever needed
    @screen = screen #
    @image = window.editor_images[image]
    @x, @y = x, y
  end
  
  def draw(screen_x, screen_y)
    @image.draw_rot(@x - screen_x + 12, @y - screen_y + 12, ZOrder::OBJECT, 0)
    # -12 is the required offset to make the object centered on a tile.
  end
end

# Map class holds and draws course and objects.
class Map
  attr_reader :width, :height, :tiles
  attr_accessor :styles, :background, :tileset
  
  def initialize(window, screen, filename)
    # Load 25x25 tiles.
    @window = window
    @screen = screen
    if !File.exists?("levels/#{filename}")
      temp_file = File.new("levels/#{filename}", "w+")
      50.times do |i|
        line = '.' * 150
        #add the styles, tileset::background::music::gravity_intensity
        line += '~~1::1::1::1' if i == 1
        temp_file.puts line
      end
      temp_file.close
    end
    $lines = File.readlines("levels/#{filename}").map { |line| line.chop }
    @styles = ($lines[1].split(/~~/, 2)[1]).split(/::/, 4)
    reload 
    # reload will also initialize the width, height, and objects fields
  end
  
  def reload
    @tileset = Image.load_tiles(@window, "images/" + Styles::TILESETS[@styles[0]], 25, 25, true)
    @background = Background.new(@window, @window.images[Styles::BACKGROUNDS[@styles[1]]])
    @height = $lines.size
    @width = $lines[0].size
    @objects = []
    
    @tiles = Array.new(@height) do |y|
      Array.new(@width) do |x|
        case $lines[y][x, 1]
        when '"'
          Tiles::TOP_LAYER[0]
        when '%'
          Tiles::BLOCK[0]
        when '_'
          Tiles::EMPTY
        else
          if OBJ_IMGS[$lines[y][x, 1]]
            @objects << CourseObject.new(@window, @screen, $lines[y][x, 1], x * Tiles::WIDTH, y * Tiles::HEIGHT)
          end
          nil
        end
      end
    end
  end
  
  def draw(screen_x, screen_y)
    @background.draw(screen_x, screen_y)
    # Advanced drawing function:
    # Draws on-screen tiles only.
    offsets_y1 = (HEIGHT.to_f/Tiles::HEIGHT.to_f).ceil
    offsets_y2 = offsets_y1 + 1
    offsets_x1 = (WIDTH.to_f/Tiles::WIDTH.to_f).ceil
    offsets_x2 = offsets_x1 + 1
    start_x, start_y = screen_x/Tiles::WIDTH, screen_y/Tiles::HEIGHT
    offs_x, offs_y = screen_x % Tiles::WIDTH, screen_y % Tiles::HEIGHT
    (offs_y == 0 ? offsets_y1 : offsets_y2).times do |y|
      (offs_x == 0 ? offsets_x1 : offsets_x2).times do |x|
        row = @tiles[y + start_y]
        if row then tile = row[x + start_x] end  # no error at edges
        if tile and tile != 0
          @tileset[tile].draw(x * Tiles::WIDTH - offs_x, y * Tiles::HEIGHT - offs_y, ZOrder::COURSE)
        elsif tile == 0
          @window.editor_images['empty.png'].draw(x * Tiles::WIDTH - offs_x, y * Tiles::HEIGHT - offs_y, ZOrder::COURSE)
        end
      end
    end
    @objects.each { |c| c.draw(screen_x, screen_y) }
  end
  
  # Solid at a given pixel position?
  def solid?(x, y)
    y < 0 || @tiles[x / Tiles::WIDTH][y / Tiles::HEIGHT]
  end

end

# Ted class handles editing functions
class Ted
  attr_reader :type
  
  def initialize(window, screen)
    @window = window
    @screen = screen
    @type = "`" # this type setting will do nothing to the map when clicked
  end
  
  def save(filename)
    file = File.new("levels/#{filename}", "w+")
    $lines[1] = $lines[1].split(/~~/, 2)[0] + '~~' + @screen.map.styles.join('::')
    file.puts($lines)
    file.close
    @screen.message = "Your level has been saved."
    Task.new(@screen, :wait => 120) { @screen.message = "" }
  end
  
  def reload
    @screen.map.reload
  end
    
  def set_type(type)
    @type = type
  end

  def set_style(mode, setting)
    map = @screen.map
    case mode
    when :tileset
      Styles::TILESETS.each do |key, val|
        map.styles[0] = key if val == setting
      end
    when :background
      Styles::BACKGROUNDS.each do |key, val|
        map.styles[1] = key if val == setting
      end
    when :music
      Styles::MUSIC.each do |key, val|
        map.styles[2] = key if val == setting
      end
    when :gravity
      map.styles[3] = setting
    end
    reload
  end
  
  def clicked(sx, sy)
    mx = @window.mouse_x
    my = @window.mouse_y
    ry = ((sy + my) / Tiles::WIDTH).floor
    rx = ((sx + mx) / Tiles::HEIGHT).floor     
    if $lines[ry] and $lines[ry][rx]
      $lines[ry][rx] = @type unless @type == '`' 
      #  ' is the "do nothing to the map" type.
    end
    reload
  end

  def add_row
    temp_string = String.new
    $lines[0].size.times { temp_string += '.' }
    $lines[$lines.size] = temp_string
    reload
  end

  def add_column
    $lines.each_with_index do |line, i|
      line = line.split(/~~/, 2)[0] if i == 1
      line += '.' # lines are actually strings 
      $lines[i] = line
    end
    reload
  end

  def cut_row
    $lines.pop
    reload
  end

  def cut_column
    $lines.each_with_index do |line, i| 
      $lines[i] = line.chop
    end
    reload
  end
  
end

# The EditorMain class is the screen that brings it all together
class EditorMain < Screen
  attr_reader :map
  attr_reader :mouse
  attr_accessor :images, :message
  
  SCROLL_DIV = 5  #affects the scroll speed, higher = slower
  SCROLL_FAC = 5  #affects the range for scrolling to begin, higher = longer

  def initialize(window, filename)
    super(window)
    @window = window
    @filename = filename
    @message = "Press \"T\" to test your level."
    Task.new(self, :wait => 120) { @message = "" }
    @map = Map.new(window, self, filename)
    @mouse = MousePointer.new(window, self)
    @mouse.set_img("pointer.png")
    @ted = Ted.new(window, self)
    # Scrolling is stored as the position of the top left corner of the screen.
    @screen_x = @screen_y = 0
    @font = Font.new(window, Gosu::default_font_name, 30)
    Instructions.new(window)
  end

  def update
    move_x = 0
    move_y = 0
    move_x += atedge("left")/SCROLL_DIV + atedge("right")/SCROLL_DIV
    move_y += atedge("up")/SCROLL_DIV + atedge("down")/SCROLL_DIV
    @tasks.reject! {|t| !t.tick} # @tasks initialized in superclass Screen
    
    #the following ensures the scrolling doesn't move offscreen
    #and sets the position of the camera
    tempscreen_x = @screen_x + move_x
    tempscreen_y = @screen_y + move_y
    if tempscreen_x <= 0
      tempscreen_x = 0
    elsif tempscreen_x >= @map.width * Tiles::WIDTH - WIDTH
     tempscreen_x = @map.width * Tiles::WIDTH - WIDTH
    end
    if tempscreen_y <= 0
      tempscreen_y = 0
    elsif tempscreen_y >= @map.height * Tiles::HEIGHT - HEIGHT
     tempscreen_y = @map.height * Tiles::HEIGHT - HEIGHT
    end
    @screen_x = tempscreen_x
    @screen_y = tempscreen_y
    if @window.button_down? Button::MsLeft and @window.button_down? Button::KbLeftShift
      @ted.clicked(@screen_x, @screen_y)
    end
    
    #the following sets the window caption to give the user information
    cx = ((@screen_x + @window.mouse_x) / Tiles::WIDTH).floor
    cy = ((@screen_y + @window.mouse_y) / Tiles::HEIGHT).floor
    case @ted.type
      when '%'
        ct = "Solid"
        @mouse.set_img('e_images/brick.png', 1)
      when '"'
        ct = "Top level"
        @mouse.set_img('e_images/brick2.png', 2)
      when '_'
        ct = "Empty"
        @mouse.set_img('e_images/empty.png')
      when '.'
        ct = "Erase"
        @mouse.set_img('e_images/eraser.png')
      else
        if OBJ_IMGS[@ted.type] 
          @mouse.set_img("e_images/" + OBJ_IMGS[@ted.type]) 
          ct = "object"
        else
          ct = " "
          @mouse.set_img('pointer.png')
        end
      end
    if !mouse_on_course?(@screen_x, @screen_y, @window.mouse_x, @window.mouse_y)
      ct += "**OUT OF BOUNDS**" 
    end
    @window.caption = "Level Constructor (#{cx}; #{cy}; #{ct})"
  end

  def draw
    @map.draw(@screen_x, @screen_y)
    @mouse.draw
    @font.draw_rel(@message, WIDTH/2, HEIGHT/2, ZOrder::TEXT, 0.5, 0.5,
                   1, 1, 0xff000000)
  end

  def button_down(id)
    if id == Button::KbEscape then kill end
    if id == Button::Kb1 then @ted.set_type("%") end
    if id == Button::Kb2 then @ted.set_type("\"") end
    if id == Button::Kb3 then @ted.set_type("_") end
    if id == Button::Kb4 then @ted.set_type(".") end
    if id == char_to_button_id('=') then @ted.add_column end
    if id == char_to_button_id('-') then @ted.add_row end
    if id == char_to_button_id('x') then @ted.cut_row end
    if id == char_to_button_id('c') then @ted.cut_column end
    if id == char_to_button_id('s') then StyleMenu.new(@window, self) end
    if id == char_to_button_id('i') then Instructions.new(@window) end
    if id == Button::MsLeft then @ted.clicked(@screen_x, @screen_y) end
    if id == Button::MsRight then SelectType.new(@window, self) end
    if id == Button::KbSpace then @ted.save(@filename) end
    if id == char_to_button_id('t')
      @ted.save(@filename)
      GameScreen.new(@window, 'test', @filename[/\d+/].to_i)
    end
  end

  def char_to_button_id(char)
    @window.char_to_button_id(char)
  end

  def set_type(type)
    @ted.set_type(type)
  end

  def set_style(mode, setting)
    @ted.set_style(mode, setting)
  end

  def atedge(dir)
    mouse_x = @window.mouse_x
    mouse_y = @window.mouse_y
    val = 0
    return val if !mouse_on_screen?(mouse_x, mouse_y)
    case dir
    when "left"
      if mouse_x.between?(0, Tiles::WIDTH*SCROLL_FAC)
        val = -(Tiles::WIDTH*SCROLL_FAC - mouse_x) #invert value to get neg num 
      end
    when "right"
      if mouse_x.between?(WIDTH - (Tiles::WIDTH*SCROLL_FAC), WIDTH)
        val = -(WIDTH - mouse_x) + (Tiles::WIDTH*SCROLL_FAC)
      end
    when "up"
      if mouse_y.between?(0, Tiles::HEIGHT*SCROLL_FAC)
        val = -(Tiles::HEIGHT*SCROLL_FAC - mouse_y)
      end
    when "down"
      if mouse_y.between?(HEIGHT - (Tiles::HEIGHT*SCROLL_FAC), HEIGHT)
        val = -(HEIGHT - mouse_y) + (Tiles::HEIGHT*SCROLL_FAC)
      end
    end
    return val
  end

  def mouse_on_screen?(x, y)
    if x >= 0 and x <= WIDTH and y >= 0 and y <= HEIGHT
      return true
    else
      return false
    end
  end

  def mouse_on_course?(sx, sy, mx, my)
    pos_x = ((sx + mx) / Tiles::WIDTH).floor
    pos_y = ((sy + my) / Tiles::HEIGHT).floor
    map_width = $lines[0].size
    map_height = $lines.size
    return true if pos_x <= map_width and pos_y <= map_height
    false
  end

end
