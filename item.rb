include Gosu
include Globals

class Item < GameObject
  attr_reader :x, :y, :width, :height

  def initialize(window, screen, type, x, y)
    super(screen)
    case type
    when :health_small
      image = "images/item_smhealth.png"
      sound = "health_collect.ogg"
      @width, @height = 25, 25
      @angle = 0
      x += 12.5
    when :bullet_item
      image = "images/item_bullet.png"
      sound = "extra_bullet.ogg"
      @width, @height = 15, 15
      @angle = 0
      x += 12.5
      y -= 5
    when :super_jump
      image = "images/item_sjump.png"
      sound = "jump_collect.ogg"
      @width, @height = 30, 30
      @angle = 0
      x += 12.5
    when :key_gold
      image = "images/key_gold.png"
      sound = "key_collect.ogg"
      @width, @height = 25, 50
      @angle = 0
      x += 12.5
      y -= 5
    when :key_silver
      image = "images/key_silver.png"
      sound = "key_collect.ogg"
      @width, @height = 25, 50
      @angle = 0
      x += 12.5
      y -= 5
    when :key_bronze
      image = "images/key_bronze.png"
      sound = "key_collect.ogg"
      @width, @height = 25, 50
      @angle = 0
      x += 12.5
      y -= 5
    when :coin
      image = "images/item_coin.png"
      sound = "coin_collect.ogg"
      @width, @height = 25, 25
      @angle = 0
      x += 12.5
      y += 12.5
    end
    @images = Image.load_tiles(window, image, @width, @height, true)
    @mode_num = @images.size
    @x, @y = x, y
    @window = window
    @screen = screen
    @sound = sound
    @type = type
    @mode = 0
    @obtained = false
    @spin = :down
    @factor_x = 1
  end

  def update
    screen_x, screen_y = @screen.screen_x, @screen.screen_y
    spot_x, spot_y = @x - screen_x, @y - screen_y
    if spot_x > - 100 and spot_x < WIDTH + 100
      if spot_y > - 100 and spot_y < HEIGHT + 100
        @mode = milliseconds / 100 % @mode_num
        case @type
        when :coin
          spin(30)
        end
      end
    end
    return false if @obtained
    true
  end

  def spin(speed)
    # if speed = 30, then a full spin in 1 second
    # the higher the speed, the slower the spin. (I know, contradictory)
    # Math.sin is measured in radians
    @factor_x = Math.sin((3.14159 * @window.time)/speed)
  end

  def draw(screen_x, screen_y)
    spot_x, spot_y = @x - screen_x, @y - screen_y
    if spot_x > - 100 and spot_x < WIDTH + 100
      if spot_y > - 100 and spot_y < HEIGHT + 100
        @images[@mode].draw_rot(@x - screen_x, @y - screen_y,ZOrder::OBJECT,
                            @angle, 0.5, 0.5, @factor_x)
      end
    end
  end

  def obj_id
    @type
  end

  def obtain
    @obtained = true
    @window.samples[@sound].play(@window.game_ops['soundfx'])
  end

end
